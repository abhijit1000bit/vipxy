//
//  API.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 23/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import Alamofire

typealias ServiceResponse = (NSDictionary?, NSError?) -> Void

let baseUrlString = "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/"


class APIController: NSObject{
    
    class var shared: APIController{
        
        struct Singleton {
            
            static let instance = APIController()
        }
        
        return Singleton.instance
    }
    
    
    func postRequestWithUrl( parameters : [String : AnyObject], UrlString : String, onCompletion : @escaping ServiceResponse ) -> Void {
        print(UrlString , parameters)
        Alamofire.request(UrlString, method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON { response in
                
                if let status = response.response?.statusCode {
                    if status == 200{
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            onCompletion(JSON, nil)
                            return
                        }
                    }
                    else{
                        onCompletion(nil, response.result.error as NSError?)
                    }
                }
        }
    }
    
    
    
    
    func getRequestWithUrl(UrlString : String, onCompletion : @escaping ServiceResponse ) -> Void {
        print(UrlString)
        Alamofire.request(UrlString, method: .get, encoding: URLEncoding.default)
            .responseJSON { response in
                
                if let status = response.response?.statusCode {
                    if status == 200{
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            onCompletion(JSON, nil)
                            return
                        }
                    }
                    else{
                        onCompletion(nil, response.result.error as NSError?)
                    }
                }
        }
    }
    
    //MARK: SIGN UP USER
    func signUpUser(fullName:String, email:String, mobile:String, password:String, onCompletion: @escaping (_ status:Bool, _ message:String, _ rid:String)-> Void){
        
        postRequestWithUrl(parameters: ["full_name":fullName as AnyObject, "email":email as AnyObject, "mobile":mobile as AnyObject, "password":password as AnyObject], UrlString: baseUrlString + "signup") { (response, error) in
            
            if response != nil{
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let uid =  response!["rid"] as? Int
                    onCompletion(true, message, "\(uid!)")
                    
                    
                }else{
                    
                    onCompletion(false, message, "")
                }
                
            }else{
                
                onCompletion(false, "Api Error", "")
            }
            
            
        }
        
    }
    
    
    
    //MARK: VERIFY USER
    
    func verifyuser(userid: String, otpReceived: String, onCompletion: @escaping(_ status: Bool, _ message: String)-> Void){
        
        postRequestWithUrl(parameters: ["uid":userid as AnyObject, "otp": otpReceived as AnyObject], UrlString: baseUrlString + "verify_account") { (response , error) in
            
            if response != nil{
                
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
   
                    onCompletion(true, message)
                    
                }else{
 
                    onCompletion(false, message)
                }
                
            }else{

                onCompletion(false, "Api Error")
            }

        }

    }
    
    //MARK: LOGIN USER
    func login(phoneOrEmail:String, password:String, onCompletion: @escaping(_ status: Bool, _ message: String, _ userData: NSDictionary)-> Void){
        postRequestWithUrl(parameters: ["email":phoneOrEmail as AnyObject, "password":password as AnyObject], UrlString:baseUrlString + "login") { (response, error) in
            
            if response != nil{
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let dataRec = response!["details"] as? NSDictionary
                    onCompletion(true, message, dataRec!)
                }else{
                    
                    onCompletion(false, message, [:])
                    
                }
            }else{
                
                onCompletion(false, "Api Error", [:])
            }
            
        }
        
        
    }
    
    //MARK: FORGOT PASSWORD
    
    func getResetPasswordLink(email:String, onCOmpletion: @escaping (_ status:Bool, _ message:String)-> Void){
        
        postRequestWithUrl(parameters: ["email":email as AnyObject], UrlString: baseUrlString + "forgotpassword") { (response, error) in
            
            if response != nil{
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    onCOmpletion(true, message)
                }else
                {
                    onCOmpletion(false, message)
                }
            }else
            {
                onCOmpletion(false, "Api Error")
                
            }
            
        }
    }
    
    
    //MARK: GET MAIN SERVICES
    func getMainServices(onCompletion : @escaping (_ status:Bool, _ message: String, _ servicesList: NSArray)-> Void){
        
        getRequestWithUrl(UrlString: baseUrlString + "get_main_services") { (response, error) in
            
            if response != nil{
                
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let Data =  response!["list"] as? NSArray
                    onCompletion(true, message, Data!)
                    
                }else
                {
                    onCompletion(false, message, [])
                }
                
            }else{
                
                onCompletion(false, "Api Error", [])
            }
        }
    }
    
    //MARK: GET BLOGS
    func getBlogs(onCompletion : @escaping (_ status:Bool, _ message: String, _ servicesList: NSArray)-> Void){
        
        getRequestWithUrl(UrlString: baseUrlString + "get_blogs") { (response, error) in
            
            if response != nil{
                
                print("jsonobject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let Data =  response!["list"] as? NSArray
                    onCompletion(true, message, Data!)
                    
                }else
                {
                    onCompletion(false, message, [])
                }
                
            }else{
                
                onCompletion(false, "Api Error", [])
            }
            
        }
        
    }

    
    
    
    
    
    
    //MARK: GET SERVICES
    func getServices(msId:String, onCompletion: @escaping(_ status: Bool, _ message: String, _ servicesList: NSArray, _ serviceName:String)-> Void){
        
        postRequestWithUrl(parameters: ["msid":msId as AnyObject], UrlString: baseUrlString + "get_services") { (response, error) in
            
            if response != nil{
                
                print("jsonObject =\(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let list = response!["list"] as? NSArray
                    let msDetailDict = response!["ms_details"] as? NSDictionary
                    let serviceName = msDetailDict!["ms_name"] as! String
                    onCompletion(true, message, list!, serviceName)
                    
                }else{
                    
                    onCompletion(false, message, [], "")
                }
                
            }else{
                
                onCompletion(false, "Api Error", [], "")
                
                
            }
            
        }
        
    }
    //MARK: SEARCH ITEM
    func searchForItem(searchText:String, onCompletion: @escaping (_ status: Bool, _ message: String, _ itemList:NSArray )-> Void){
        
        postRequestWithUrl(parameters: ["search":searchText as AnyObject], UrlString: baseUrlString + "sds") { (response, error) in
            
            if response != nil {
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let itemList = response!["list"] as? NSArray
                 
                    onCompletion(true, message, itemList!)
                    
                }else{
                    
                    onCompletion(false, message, [])
                }
            }else{
                
                onCompletion(false, "APi Error", [])
            }
            
        }
        
    }
    //MARK:  GET ITEMS WITH SUB SERVICES
    //pas subid
    func getItemsWithSubServices(sid:String, onCompletion: @escaping(_ status: Bool, _ message: String, _ itemList:NSArray, _ hasSubServicesThenItem:String, _ subServiceName:String)-> Void){
        
        postRequestWithUrl(parameters: ["sid":sid as AnyObject], UrlString: baseUrlString + "get_item_with_subservices") { (response, error) in
            
            if response != nil{
                
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let sDetailsDict = response!["s_details"] as? NSDictionary
                    let subServiceName = sDetailsDict!["s_name"] as? String
                    let hasSubServicesThenItems = sDetailsDict!["has_subservices_then_item"] as? String
                    let itemList = response!["list"] as? NSArray

                    onCompletion(true, message, itemList!, hasSubServicesThenItems!, subServiceName!)
                }else{
                    
                    onCompletion(false, message, [], "", "")
                }
            }else{
                
                onCompletion(false, "Api Error", [], "" ,"")
            }
            
        }
        
    }
    
    //MARK: GET ADDRESS LISTING
    func getAddressList(uid:String, onCompletion: @escaping(_ status: Bool, _ message: String, _ addressList:NSArray)-> Void){
        
        postRequestWithUrl(parameters: ["uid":uid as AnyObject], UrlString: baseUrlString + "ssdf") { (response, error) in
            
            if response != nil{
                
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    
                    
                    
                    
                    
                }else{
                    
                    onCompletion(false, message, [])
                }
                
                
            }else{
                
                onCompletion(false, "Api Error", [])
            }
            
        }
        
        
    }
    
    //MARK: GET PROFILE
    func getProfile(uid:String, onCompletion: @escaping(_ status : Bool, _ message : String, _ email:String, _ contactNum:String, _ password:String, _ profileUrl:String) -> Void){
        
        postRequestWithUrl(parameters: ["uid": uid as AnyObject ], UrlString: baseUrlString + "get_profile") { (response, error) in
            
            if response != nil{
                
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    let detailDict = response!["detail"] as! NSDictionary
                    let email = detailDict["email"] as! String
                    let contactNum = detailDict["mobile"] as! String
                    let passwd = detailDict["password"] as! String
                    let profile = detailDict["profile_photo"] as! String
                    
                    onCompletion(true, message, email, contactNum, passwd, profile)
                    
                    
                }else{
                    
                    onCompletion(false, message, "", "", "", "")
                }
                
            }else{
                
                onCompletion(false, "Api Error", "", "", "", "")
            }
            
        }
        
    }
    //MARK: UPDATE PROFILE PHOTO
    
    func updateUserDetails(userId:String, image:UIImage, onCompletion : @escaping (_ status : Bool, _ message:String) -> Void){
        
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            //multipartFormData.append(profileImg, withName: "profile_img")
            let imageData = image.pngData()
            multipartFormData.append(imageData!, withName: "profile_photo", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
            multipartFormData.append(userId.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName :"uid")
            
        },
                         usingThreshold:UInt64.init(),
                         to:baseUrlString + "change_profile_photo",
                         method:.post,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    debugPrint(response)
                                    if let status = response.response?.statusCode {
                                        if status == 200{
                                            if let result = response.result.value {
                                                let JSON = result as! NSDictionary
                                                
                                                
                                                
                                                print("jsonObject =\(String(describing: JSON))")
                                                let message = JSON["message"] as! String
                                                if JSON["message_code"] as? Int  == 1
                                                {
                                                    //let Data = response!["Details"] as? NSArray
                                                    onCompletion(true, message)
                                                }
                                                else
                                                {
                                                    onCompletion(false, message)
                                                }
                                                
                                                return
                                            }
                                        }
                                        else{
                                            onCompletion(false, response.result.error as! String )
                                        }
                                    }
                                    
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                //onCompletion(false, encodingError as! String)
                            }
        })
        
        
    }
    
    func updateTheField(field:String, updatedValue:String, onCompletion: @escaping(_ status:Bool, _ message:String)-> Void){
        var urlStr = baseUrlString
        
        if field == "Change Password"{
            urlStr = urlStr + "change_password"
            postRequestWithUrl(parameters: ["uid":Utility.getuserId() as AnyObject, "new_password":updatedValue as AnyObject], UrlString: urlStr) { (response, error) in
                
                if response != nil {
                    
                    print("jsonObject = \(String(describing: response))")
                    let message = response!["message"] as! String
                    if response!["message_code"] as? Int == 1
                    {
                        
                        
                        onCompletion(true, message)
                        
                        
                    }else{
                        
                        onCompletion(false, message)
                    }
                    
                }else{
                    onCompletion(false, "Api Error")

                    
                }
                
            }
        }
        if field == "Email" {
            urlStr = urlStr + "update_email"
            postRequestWithUrl(parameters: ["uid":Utility.getuserId() as AnyObject, "email":updatedValue as AnyObject], UrlString: urlStr) { (response, error) in
                if response != nil {
                    print("jsonObject = \(String(describing: response))")
                    let message = response!["message"] as! String
                    if response!["message_code"] as? Int == 1
                    {
                        
                        
                        onCompletion(true, message)
                        
                        
                    }else{
                        
                        onCompletion(false, message)
                    }
                }else{
                    onCompletion(false, "Api Error")
                    
                    
                }
            }
        }
        if field == "Contact Number"{
            urlStr = urlStr + "update_mobile"
            postRequestWithUrl(parameters: ["uid":Utility.getuserId() as AnyObject, "mobile":updatedValue as AnyObject], UrlString: urlStr) { (response, error) in
                if response != nil {
                    
                    print("jsonObject = \(String(describing: response))")
                    let message = response!["message"] as! String
                    if response!["message_code"] as? Int == 1
                    {
                        
                        
                        onCompletion(true, message)
                        
                        
                    }else{
                        
                        onCompletion(false, message)
                    }
                }else{
                    onCompletion(false, "Api Error")
                    
                    
                }
            }
        }
        

    }
    
    

    //MARK: ADD NEW ADDRESS
    func addNewAddress(uid:String, tag:String, location:String, houseNumber:String, landmark:String, onCompletion: @escaping(_ status : Bool, _ message: String)-> Void){
        
        postRequestWithUrl(parameters: ["uid":uid as AnyObject, "tag":tag as AnyObject, "location":location as AnyObject, "house_no":houseNumber as AnyObject, "landmark":landmark as AnyObject], UrlString: baseUrlString + "add_address") { (response, error) in
            if response != nil{
                
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1
                {
                    
                    
                    onCompletion(true, message)
                    
                    
                }else{
                    
                    onCompletion(false, message)
                }
                
                
            }else{
                
                onCompletion(false, "Api Error")
            }
            
            
        }
    }
    
    //MARK: PLACE THE FINAL ORDER
    func placeTheFinalOrder(parameters:String, uid:String, addressId:String, serviceDate:String, serviceTime:String, paymentType:String, paymentTxnId:String, msId:String, onCompletion:@escaping(_ status:Bool, _ message:String)-> Void){
        //insert msid intoi the place order api
        postRequestWithUrl(parameters:["item_str":parameters as AnyObject,"uid":uid as AnyObject,"add_id":addressId as AnyObject,"service_dt":serviceDate as AnyObject,"service_time":serviceTime as AnyObject,"payment_type":paymentType as AnyObject,"payment_txnid":paymentTxnId as AnyObject, "msid":msId as AnyObject], UrlString: baseUrlString + "place_order") { (response, error) in
            
            if response != nil{
                
                print("jsonObject = \(String(describing: response))")
                let message = response!["message"] as! String
                if response!["message_code"] as? Int == 1{
                    
                    onCompletion(true, message)
                }else{
                    onCompletion(false, message)
                }
            }else{
                
                onCompletion(false, "Api Error")
            }
        }
        
    }

    
    
}
