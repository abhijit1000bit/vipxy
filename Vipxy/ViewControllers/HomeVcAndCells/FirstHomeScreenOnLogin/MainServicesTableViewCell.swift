//
//  MainServicesTableViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 23/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class MainServicesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var serviceImageView: UIImageView!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    
    
}
