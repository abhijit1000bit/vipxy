//
//  HomeScreenVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 23/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SideMenu

class HomeScreenVc: BaseViewController, UITableViewDelegate, UITableViewDataSource, FSPagerViewDataSource, FSPagerViewDelegate, UISearchBarDelegate{
    //var basicArray: [[String:Any]] = [[:]]
    
    @IBOutlet weak var searchBarHoldingView: UIView!
     var searchBaar:UISearchBar = UISearchBar()
    
    var servicesArray : [[Service]] = [[Service]]()
    var searchItemArray : [Item] = [Item]()
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.automaticSlidingInterval = 3.0
            //pagerView.transformer = FSPagerViewTransformer(type: .linear)
            //pagerView.itemSize = CGSize(width: 300, height: 180)
            //pagerView.interitemSpacing = 10
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    

    @IBOutlet weak var pageControl: FSPageControl!{
        
        didSet{
            
            self.pageControl.numberOfPages = blogsArray.count
            self.pageControl.contentHorizontalAlignment = .center
            pageControl.setStrokeColor(.gray, for: .normal)
            pageControl.setStrokeColor(.black, for: .selected)
            //pageControl.setFillColor(.gray, for: .normal)
            //pageControl.setFillColor(.white, for: .selected)
             pageControl.itemSpacing = 10.0
            //pageControl.interitemSpacing = 2.0
            
            
        }
        
        
    }
    
    
    
    
    
    
    let cellId = "CellId"
    
    
    var mainServicesArray : [[MainService]] = [[MainService]]()
    var basicArray: [[String:Any]] = [[:]]
    
    var blogsArray : [[Blog]] = [[Blog]]()
    override func viewDidLoad() {

        super.viewDidLoad()
        getMainServices()
        tableView.register(UINib.init(nibName: "MainServicesTableViewCell", bundle: nil), forCellReuseIdentifier: "MainServicesTableViewCell")
        //getBlogs()
        
        //tableView.register(Cell.self, forCellReuseIdentifier: "CellId")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        //pageControlN.numberOfPages = upcomingBannerArray.count
        //pageControlN.contentHorizontalAlignment = .right
        
        //self.pageControl.numberOfPages = blogsArray.count
        //self.pageControl.contentHorizontalAlignment = .right
        self.navigationItem.title = "VIPXY"
        
        var frm =  searchBaar.frame
        frm = CGRect(x: 50, y: 10, width: 284, height: 37)
        searchBaar.frame = frm
        self.searchBarHoldingView.addSubview(searchBaar)
        searchBaar.placeholder = "Beauty, Cleaning, Health, Salon etc."
        searchBaar.setPlaceholder(textColor: .lightGray)
        
        
        searchBaar.tintColor = .black
        searchBaar.backgroundColor = .white
        //searcHBaar.setSearchFieldBackgroundImage(UIImage(named: "signin_username_bg"), for: .normal)
        //searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBaar.clipsToBounds = true
        searchBaar.layer.cornerRadius = 2
        searchBaar.barTintColor = .black
        //searcHBaar.setNewcolor(color: .yellow)
        searchBaar.setPlaceholder(textColor: UIColor(red:0.82, green:0.82, blue:0.83, alpha:1.0))
        searchBaar.setTextField(color: .white)
        
        searchBaar.delegate = self
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = .black
        if Utility.getLoginStatus(){
            
            let menuButton: UIButton = UIButton(type: .custom)
            menuButton.frame = CGRect(x: 0, y: 10, width: 20, height: 20)
            menuButton.setImage(UIImage(named: "menu"), for: .normal)
            menuButton.addTarget(self, action:#selector(self.SideMenuAction), for: .touchUpInside)
            let menuBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: menuButton)
            //let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "slider_button"), style: .plain, target: self, action: #selector(BaseViewController.SideMenuAction))
            
            self.navigationItem.setLeftBarButton(menuBarButtonItem,animated: true)
            
        }
        
        
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()  //very very imp , otherwise the pager tab bar collection view cells are not evenly distributed
        showSideMenu(view: self.view) // show sidemenu
    }
    
    //    override func viewWillLayoutSubviews() {
    //
    //        super.viewWillLayoutSubviews()
    //        showSideMenu(view: self.view)
    //    }
    
    @objc override func SideMenuAction()
    {
        
        //        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //        let sideMenuObj : SideMenuViewController! = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        //        let menu = SideMenuNavigationController(rootViewController: sideMenuObj)
        //        present(menu, animated: true, completion: nil)
        
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }
    
    override func showSideMenu(view: UIView) {
        // Define the menus
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let sideMenuObj : SideMenuViewController! = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        let menuLeftNavigationController = SideMenuNavigationController(rootViewController: sideMenuObj)
        menuLeftNavigationController.leftSide = true
        //menuLeftNavigationController.presentationStyle.backgroundColor = .black
        
        //menuLeftNavigationController.presentationStyle.menuStartAlpha = 1
        //menuLeftNavigationController.presentationStyle.menuOnTop = false
        //menuLeftNavigationController.presentationStyle.menuTranslateFactor = 0
        //menuLeftNavigationController.presentationStyle.menuScaleFactor = 1
        //menuLeftNavigationController.presentationStyle.onTopShadowOpacity = 0
        //menuLeftNavigationController.presentationStyle.onTopShadowOffset = .zero
        //menuLeftNavigationController.presentationStyle.presentingEndAlpha = 1
        //menuLeftNavigationController.presentationStyle.presentingTranslateFactor = 0
        //menuLeftNavigationController.presentationStyle.presentingScaleFactor = 1
        //menuLeftNavigationController.presentationStyle.presentingParallaxStrength = .zero
        
        menuLeftNavigationController.presentationStyle.onTopShadowRadius = 5.0
        menuLeftNavigationController.blurEffectStyle = .dark
        menuLeftNavigationController.presentationStyle.onTopShadowColor = .black
        //sideMenuObj.delget = self
        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        // let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController = menuLeftNavigationController
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        
        //        SideMenuManager.menuAddPanGestureToPresent(toView: view) //self.navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: SideMenuManager.PresentDirection(rawValue: 1)!)
        
        
        
        
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - (UIScreen.main.bounds.width/7)
        SideMenuManager.default.menuDismissOnPush = true
        //        SideMenuManager.menuEnableSwipeGestures = false
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        //SideMenuManager.default.menuPushStyle = .subMenu
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
    }
    
    
    override func closeMenu()
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: PAGE CONTROL DELEGATE AND DATASOURCE METHODS
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return blogsArray.count
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        let blog = blogsArray[index]
        blogRedirect(blogId: blog.first!.blogId)
        
    }
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let blogh = blogsArray[index]
        if let categoryImageUrl = blogh.first?.blogImage{
            if let imageURL:URL = URL(string: categoryImageUrl) {
                cell.imageView?.sd_setShowActivityIndicatorView(true)
                cell.imageView?.sd_setIndicatorStyle(.gray)
                cell.imageView?.sd_setImage(with: imageURL) { (image, error, cache, urls) in
                    
                }
                
            }
            
        }
        //cell.imageView?.image = UIImage(named: self.imageNames[index])
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        print("inside pager cell == \(blogh.first?.blogTitle)")
        
        //cell.textLabel?.text = blogh.first?.blogTitle
        
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    
    
    //MARK: UITABLEVIEW DATASOURCE DELEGATE
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        

        return  self.mainServicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        
//        if indexPath.section == 1{
        
            
            let service = mainServicesArray[indexPath.row]
            
            let serviceCell = tableView.dequeueReusableCell(withIdentifier: "MainServicesTableViewCell", for: indexPath) as! MainServicesTableViewCell
            
            serviceCell.nameLbl.text = service.first?.msName
            serviceCell.descriptionLbl.text = service.first?.msDescription
            serviceCell.serviceImageView.sd_setShowActivityIndicatorView(true)
            serviceCell.serviceImageView.sd_setIndicatorStyle(.gray)
            serviceCell.serviceImageView.sd_setImage(with: URL(string: service.first!.msImage)) { (image, error, cache, urs) in
                
            }
        
        
        
        
            serviceCell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            return serviceCell
            
            
            
//        }else{
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! Cell
//
//            cell.passBannerData(bannerArray: blogsArray)
//
//            return cell
//
//
//        }
            

            
    }
    
    //On clicking a particular cell, navigate to the detailed screen
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        let mainService = mainServicesArray[indexPath.row]
        
        Utility.setMainServiceId(mainService.first!.msId)
    
        
        
        getService(msId: mainService.first!.msId, titleOfNavBar: mainService.first!.msName)
        
    }
    
    
    
    
    
    func getService(msId: String, titleOfNavBar:String){
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().getServices(msId: msId) { (status, message, serviceList, serviceName) in
                
                if status == true{
                    
                    self.stopLoading()
                    
                    print("received services array = \(serviceList)")
                    
                    self.basicArray = serviceList as? [[String:Any]] ?? [[:]]
                    self.servicesArray = [[Service]]()
                    for serv in self.basicArray{
                        let service = Service(json: serv)
                        self.servicesArray.append([service])
                    }
                    
                    print("modeled services array = \(self.servicesArray)")
                    
                    
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let detailedServicesScreenVc = storyBoard.instantiateViewController(withIdentifier: "DetailedServicesVc") as! DetailedServicesVc
                  
                    detailedServicesScreenVc.servicesArray = self.servicesArray
                    detailedServicesScreenVc.serviceName = serviceName
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    self.navigationItem.backBarButtonItem = backItem
                    self.navigationController?.pushViewController(detailedServicesScreenVc, animated: true)
                    
                    


                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
                
            }
            
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func getMainServices(){
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            APIController().getMainServices { (status, message, servicesArray) in
                
                if status == true{
                    self.stopLoading()
                    
                    print("received services array = \(servicesArray)")
                    
                    self.basicArray = servicesArray as? [[String:Any]] ?? [[:]]
                    self.mainServicesArray = [[MainService]]()
                    
                    for service in self.basicArray{
                        
                        let serv = MainService(json: service)
                        self.mainServicesArray.append([serv])
                        
                    }
                    
                    print("modeled services arry = \(self.mainServicesArray)")
                    
                    self.tableView.reloadData()
                    
//                    APIController().getBlogs { (status, message, blogsAray) in
//                        
//                        
//                        if status ==  true
//                        {
//                            self.stopLoading()
//                            
//                            print("received blogs array = \(blogsAray)")
//                            
//                            self.basicArray = blogsAray as? [[String:Any]] ?? [[:]]
//                            self.blogsArray = [[Blog]]()
//                            
//                            for blog in self.basicArray{
//                                
//                                let bloog = Blog(json: blog)
//                                self.blogsArray.append([bloog])
//                                
//                            }
//                            
//                            print("blogs arry = \(self.blogsArray)")
//                            
//                            
//                            self.tableView.reloadData()
//                            
//                            
//                        }else{
//                            
//                            self.stopLoading()
//                            self.showAlertView(title: "Failure", message: message)
//                            
//                        }
//                        
//                        
//                    }

                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
            }
            
            
        }
        
        
    }
    
    func getBlogs(){
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().getBlogs { (status, message, blogsAray) in
                
                
                if status ==  true
                {
                    self.stopLoading()
                    
                    print("received blogs array = \(blogsAray)")
                    
                    self.basicArray = blogsAray as? [[String:Any]] ?? [[:]]
                    self.blogsArray = [[Blog]]()
                    
                    for blog in self.basicArray{
                        
                        let bloog = Blog(json: blog)
                        self.blogsArray.append([bloog])
                        
                    }
                    
                    print("blogs arry = \(self.blogsArray)")
                    
                    
                    
                    
                    
                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
                
            }
            
            
            
        }
        
        
    }
    
    //MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""
        {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        
    }
    
    //func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    // hideSearchBar()
    //}
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        
        searchItem(searchText: searchBar.text!)
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        
        
        print("keyboard search clicked")
    }
    
    func blogRedirect(blogId:String){
        if(!ReachabilitySwift.isConnectedToNetwork()){
            //self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            
            startLoading()
            var request = URLRequest(url: URL(string: "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/items_from_blog")!)
            request.httpMethod = "POST"
            let postString = "bid=\(blogId)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil else{
                    
                    DispatchQueue.main.async {
                        self.stopLoading()
                    }
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    
                    
                }else{
                    //parse
                    self.subServiceResponse(data: data)
                }
                DispatchQueue.main.async {
                    self.stopLoading()
                }
                
            }
            task.resume()
            
        }
        
    }
    
    
    
    func searchItem(searchText:String){
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            //self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            
            startLoading()
            var request = URLRequest(url: URL(string: "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/search_items")!)
            request.httpMethod = "POST"
            let postString = "search=\(searchText)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil else{
                    
                    DispatchQueue.main.async {
                        self.stopLoading()
                    }
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    
                    
                }else{
                    //parse
                    self.subServiceResponse(data: data)
                }
                DispatchQueue.main.async {
                    self.stopLoading()
                }
                
            }
            task.resume()
            
        }
    }
    
    func subServiceResponse(data: Data){
        
        DispatchQueue.main.async {
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                let str = json!["message"] as? String
                let value = json!["message_code"] as? Int
                if (value == 1){
                    let jsonResult = try JSONDecoder().decode(OuterJsonSearchItems.self, from: data)
                    print("final json result JsonItems  == \(jsonResult.list)")
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let itemListScreenVc = storyBoard.instantiateViewController(withIdentifier: "ItemsListvc") as! ItemsListvc
                    
                    itemListScreenVc.itemList = jsonResult.list
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    self.navigationItem.backBarButtonItem = backItem
                    self.navigationController?.pushViewController(itemListScreenVc, animated: true)
                    
                }else{
                    
                    self.showAlertView(title: "Alert", message: str!)
                }
                
            }catch{
                
                print("Error with Json: \(error)")
            }

        }
        
        
    }
    
    
    
    
    
}


//class Cell: UITableViewCell, FSPagerViewDelegate, FSPagerViewDataSource{
//
//
//    let cellId = "CellId"; // same as above unique id
//    fileprivate let imageNames = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg"]
//    fileprivate var numberOfItems = 7
//
//    lazy var fmOne = CGRect(x: 0, y: 0, width: self.frame.width, height: 180)
//    lazy var pagerViewN = FSPagerView(frame: fmOne)
//
//    lazy var fmTwo = CGRect(x: self.frame.width/2 + 50, y: self.frame.height - 25, width: 30, height: 30)
//    lazy var pageControlN = FSPageControl(frame: fmTwo)
//
//    lazy var upcomingBannerArray:[[Blog]] = [[Blog]]()
//
//    //weak var launchuploadDelegate:launchUploadVideoVc?
//
//    func passBannerData(bannerArray: [[Blog]]) {
//
//        upcomingBannerArray = bannerArray
//
//        print("banner array received == \(upcomingBannerArray)")
//        pageControlN.numberOfPages = upcomingBannerArray.count
//        pagerViewN.collectionView.reloadData()
//    }
//
//
//
//
////    override init(frame: CGRect) {
////        super.init(frame: frame);
////
////        setupViews();
////        //collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId); //register custom UICollectionViewCell class.
////        // Here I am not using any custom class
////
////    }
//
//
//
//
//
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//
//        super.init(style: .default, reuseIdentifier: "nil")
//        setupViews()
//    }
//
//
//
//
//    func setupViews(){
//        //        addSubview(collectionView);
//        //
//        //        collectionView.delegate = self;
//        //        collectionView.dataSource = self;
//        //
//        //        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true;
//        //        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true;
//        //        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true;
//        //        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true;
//
//        //frame1
//
//        //let fmOne = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
//
//        // Create a pager view
//        //let pagerView = FSPagerView(frame: fmOne)
//        pagerViewN.dataSource = self
//        pagerViewN.delegate = self
//        pagerViewN.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
//        addSubview(pagerViewN)
//        // Create a page control
//        //let fmTwo = CGRect(x: self.frame.width/2 - 10, y: self.frame.height - 15, width: 30, height: 30)
//        pagerViewN.automaticSlidingInterval = 3.0
//
//        //let pageControl = FSPageControl(frame: fmTwo)
//        pageControlN.numberOfPages = upcomingBannerArray.count
//        pageControlN.contentHorizontalAlignment = .right
//
//        addSubview(pageControlN)
//
//    }
//
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//
//    public func numberOfItems(in pagerView: FSPagerView) -> Int {
//        return self.upcomingBannerArray.count
//    }
//
//    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
//        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
//
//        let compObj = upcomingBannerArray[index]
//        if let categoryImageUrl = compObj.first?.blogImage{
//            if let imageURL:URL = URL(string: categoryImageUrl) {
//                cell.imageView?.sd_setShowActivityIndicatorView(true)
//                cell.imageView?.sd_setIndicatorStyle(.gray)
//                cell.imageView?.sd_setImage(with: imageURL) { (image, error, cache, urls) in
//
//                }
//
//            }
//
//        }
//        //cell.imageView?.image = UIImage(named: self.imageNames[index])
//        cell.imageView?.contentMode = .scaleAspectFill
//        cell.imageView?.clipsToBounds = true
//
//        print("inside pager cell == \(compObj.first?.blogTitle)")
//
//        cell.textLabel?.text = compObj.first?.blogTitle
//
//
//        //cell.imageView?.image = UIImage(named: self.imageNames[index])
//        //cell.imageView?.contentMode = .scaleAspectFill
//        //cell.imageView?.clipsToBounds = true
//        //cell.textLabel?.text = index.description+index.description
//        return cell
//    }
//    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
//        pagerView.deselectItem(at: index, animated: true)
//        pagerView.scrollToItem(at: index, animated: true)
//
//        let comp = upcomingBannerArray[index]
//
//        print("status of competn clicked == \(comp.first!.blogTitle)")
//
//        //launchuploadDelegate?.passCompetionObjectTolaunchUploadVideoVc(sender: self, competion: comp, compStatus: comp.first!.status)
//        //let comp = competitionsArray[indexPath.row]
//        //let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        //let uploadYourVideoVc = storyBoard.instantiateViewController(withIdentifier: "UploadYourVideoVc") as! UploadYourVideoVc
//        //uploadYourVideoVc.competitionObject = comp
//        //uploadYourVideoVc.competitionActiveStatus = comp.first!.status
//        //self.navigationController?.pushViewController(uploadYourVideoVc, animated: true)
//    }
//
//    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
//        self.pageControlN.currentPage = targetIndex
//    }
//
//    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
//        self.pageControlN.currentPage = pagerView.currentIndex
//    }
//}
