//
//  SubSevicesThenItemsVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 26/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import ScrollableSegmentedControl
import SDWebImage



class SubSevicesThenItemsVc: BaseViewController, UITableViewDelegate, UITableViewDataSource, changeThePurchaseCountDelegate, oneAmongMultiplePackSelectedDelegate {
    
    
    func returnBackSelectedPack(itemId: String, packId: String, genderSelected: String, countReceived: Int) {
        
        
        if segmentedControl.selectedSegmentIndex == self.subServicesThenItemList.count{
            
            if let row = addOnArray.firstIndex(where: {$0.itemId == itemId}) {
                var particularSelectedItem = addOnArray[row]
                particularSelectedItem.count = countReceived
                addOnArray[row] = particularSelectedItem
                
                if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                }){
                    var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                    
                    //if particularSelectedItem.count < countReceived{
                    //plus clicked
                    print("original string = \(particularSelectedItem.multiplePackEncoding)")
                    particularSelectedItem.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                    particularSelectedItem.count = countReceived
                    addOnArray[row] = particularSelectedItem
                    print("new string = \(particularSelectedItem.multiplePackEncoding)")
                    
                    particularItemInChosenArray.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                    particularItemInChosenArray.count = countReceived
                    self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                    //}
                    self.updateSummaryView()
                    
                    
                }else{
                    
                    
                    particularSelectedItem.multiplePackEncoding = "\(packId),\(genderSelected)X"
                    particularSelectedItem.count = countReceived
                    addOnArray[row] = particularSelectedItem
                    self.chosenItemArray.append(particularSelectedItem)
                    print("multiple pack iutem added = \(particularSelectedItem)")
                    self.updateSummaryView()
                }
                
                //            if particularSelectedItem.count > countReceived{
                //                //minus clicked
                //
                //
                //
                //            }
                //
                //            if particularSelectedItem.count < countReceived{
                //                //plus ckicked
                //
                //                //new entry
                //                if particularSelectedItem.count == 0{
                //                    //insert new item
                //                    //insert selected string
                //                    self.chosenItemArray.append(particularSelectedItem)
                //                    particularSelectedItem.multiplePackEncoding = "\(packId)\(genderSelected)X"
                //                    itemList[row] = particularSelectedItem
                //
                //                }else{
                //                    //modify the count
                //                    //modify the selected string array
                //
                //                }
                //
                //            }
                
                if chosenItemArray.count != 0{
                    
                    summaryView.isHidden = false
                    
                }else{
                    
                    summaryView.isHidden = true
                }
                
                DispatchQueue.main.async {
                    
                    self.itemTableView.reloadData()
                }
            }
            
            
        }else{
            
            if let row = self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray.firstIndex(where: {$0.itemId == itemId}) {
                var particularSelectedItem = self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row]
                particularSelectedItem.count = countReceived
                self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                
                if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                }){
                    var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                    
                    //if particularSelectedItem.count < countReceived{
                    //plus clicked
                    print("original string = \(particularSelectedItem.multiplePackEncoding)")
                    particularSelectedItem.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                    particularSelectedItem.count = countReceived
                    self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                    print("new string = \(particularSelectedItem.multiplePackEncoding)")
                    
                    particularItemInChosenArray.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                    particularItemInChosenArray.count = countReceived
                    self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                    //}
                    self.updateSummaryView()
                    
                    
                }else{
                    
                    
                    particularSelectedItem.multiplePackEncoding = "\(packId),\(genderSelected)X"
                    particularSelectedItem.count = countReceived
                    self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                    self.chosenItemArray.append(particularSelectedItem)
                    print("multiple pack iutem added = \(particularSelectedItem)")
                    self.updateSummaryView()
                }
                
                //            if particularSelectedItem.count > countReceived{
                //                //minus clicked
                //
                //
                //
                //            }
                //
                //            if particularSelectedItem.count < countReceived{
                //                //plus ckicked
                //
                //                //new entry
                //                if particularSelectedItem.count == 0{
                //                    //insert new item
                //                    //insert selected string
                //                    self.chosenItemArray.append(particularSelectedItem)
                //                    particularSelectedItem.multiplePackEncoding = "\(packId)\(genderSelected)X"
                //                    itemList[row] = particularSelectedItem
                //
                //                }else{
                //                    //modify the count
                //                    //modify the selected string array
                //
                //                }
                //
                //            }
                
                if chosenItemArray.count != 0{
                    
                    summaryView.isHidden = false
                    
                }else{
                    
                    summaryView.isHidden = true
                }
                
                DispatchQueue.main.async {
                    
                    self.itemTableView.reloadData()
                }
            }
            
        }
        
        


    }
    
    
    
    func changePurchaseCount(itemId: String, itemCount: Int) {
        
        if segmentedControl.selectedSegmentIndex == self.subServicesThenItemList.count{
            
            if let row = self.addOnArray.firstIndex(where: {$0.itemId == itemId}) {
                
                
                var particularSelectedItem = addOnArray[row]
                ///////////////////////////////
                if particularSelectedItem.hasDiffPack == "Yes"{
                    
                    //minus clicked
                    if particularSelectedItem.count > itemCount{
                        
                        //check if itemCount  == 0
                        if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                        }){
                            var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                            if itemCount == 0 {
                                
                                particularSelectedItem.multiplePackEncoding = ""
                                particularSelectedItem.count = itemCount
                                addOnArray[row] = particularSelectedItem
                                self.chosenItemArray.remove(at: rowInChosenItemArray)
                                self.updateSummaryView()
                            }else{
                                
                                print("original string = \(particularSelectedItem.multiplePackEncoding)")
                                
                                var compAray = particularSelectedItem.multiplePackEncoding.components(separatedBy: "X")
                                
                                print("comp array original = \(compAray)")
                                
                                var last = compAray.removeLast()
                                
                                print("new comp array = \(compAray)")
                                
                                var secodLast = compAray.removeLast()
                                print("new comp array finally = \(compAray)")
                                var strinRep = compAray.joined(separator: "X")
                                strinRep.append("X")
                                print("new string = \(strinRep)")
                                particularSelectedItem.multiplePackEncoding = strinRep
                                
                                particularSelectedItem.count = itemCount
                                self.addOnArray[row] = particularSelectedItem
                                particularItemInChosenArray.multiplePackEncoding = strinRep
                                
                                particularItemInChosenArray.count = itemCount
                                
                                self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                                self.updateSummaryView()
                            }
                            
                        }
                        
                        
                    }else{
                        
                        //self.showMultiplePacksAlert(packArray: particularSelectedItem.packArray, itemId:itemId)
                        //Show an alert view controller and then sheow three radio buttons....
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertForMultiplePacksVc") as! AlertForMultiplePacksVc
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        //myAlert.mainMessage = title
                        //myAlert.subMessage = message
                        myAlert.packsArrayReceived = particularSelectedItem.packArray
                        myAlert.itemId = itemId
                        myAlert.purchaseDelegate = self
                        myAlert.countReceived = itemCount
                        self.present(myAlert, animated: true, completion: nil)
                        
                        let originalCount = particularSelectedItem.count
                        if originalCount == 0{
                            particularSelectedItem.count = 0
                            addOnArray[row] = particularSelectedItem
                            DispatchQueue.main.async {
                                
                                self.itemTableView.reloadData()
                            }
                            
                        }
                        
                    }
                    
                    
                    
                    
                }else{
                    
                    /////////////////////////////////
                    particularSelectedItem.count = itemCount
                    addOnArray[row] = particularSelectedItem
                    
                    print("count changed == \(subServicesThenItemList)")
                    
                    //check if the count is fetched is zero or not if zero remove the item from the array else fe
                    
                    //etche the
                    if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                    }){
                        var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                        if itemCount == 0{
                            
                            self.chosenItemArray.remove(at: rowInChosenItemArray)
                            self.updateSummaryView()
                            
                        }else{
                            
                            particularItemInChosenArray.count = itemCount
                            self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                            self.updateSummaryView()
                        }
                        
                    }else{
                        
                        self.chosenItemArray.append(particularSelectedItem)
                        self.updateSummaryView()
                        
                    }
                    
                }
                
                //show hide of the summary view:
                //If the item count is not zero show the summary. view as soon as the item count is zero then hide the summary view after the item is removed from the array
                if chosenItemArray.count != 0{
                    
                    summaryView.isHidden = false
                    
                }else{
                    
                    summaryView.isHidden = true
                }

                
                
            }
            
            
        }else{
            
            if let row = self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray.firstIndex(where: {$0.itemId == itemId}) {
                
                
                var particularSelectedItem = subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row]
                ///////////////////////////////
                if particularSelectedItem.hasDiffPack == "Yes"{
                    
                    //minus clicked
                    if particularSelectedItem.count > itemCount{
                        
                        //check if itemCount  == 0
                        if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                        }){
                            var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                            if itemCount == 0 {
                                
                                particularSelectedItem.multiplePackEncoding = ""
                                particularSelectedItem.count = itemCount
                                self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                                self.chosenItemArray.remove(at: rowInChosenItemArray)
                                self.updateSummaryView()
                            }else{
                                
                                print("original string = \(particularSelectedItem.multiplePackEncoding)")
                                
                                var compAray = particularSelectedItem.multiplePackEncoding.components(separatedBy: "X")
                                
                                print("comp array original = \(compAray)")
                                
                                var last = compAray.removeLast()
                                
                                print("new comp array = \(compAray)")
                                
                                var secodLast = compAray.removeLast()
                                print("new comp array finally = \(compAray)")
                                var strinRep = compAray.joined(separator: "X")
                                strinRep.append("X")
                                print("new string = \(strinRep)")
                                particularSelectedItem.multiplePackEncoding = strinRep
                                
                                particularSelectedItem.count = itemCount
                                self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                                particularItemInChosenArray.multiplePackEncoding = strinRep
                                
                                particularItemInChosenArray.count = itemCount
                                
                                self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                                self.updateSummaryView()
                            }
                            
                        }
                        
                        
                    }else{
                        
                        //self.showMultiplePacksAlert(packArray: particularSelectedItem.packArray, itemId:itemId)
                        //Show an alert view controller and then sheow three radio buttons....
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertForMultiplePacksVc") as! AlertForMultiplePacksVc
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        //myAlert.mainMessage = title
                        //myAlert.subMessage = message
                        myAlert.packsArrayReceived = particularSelectedItem.packArray
                        myAlert.itemId = itemId
                        myAlert.purchaseDelegate = self
                        myAlert.countReceived = itemCount
                        self.present(myAlert, animated: true, completion: nil)
                        
                        let originalCount = particularSelectedItem.count
                        if originalCount == 0{
                            particularSelectedItem.count = 0
                            self.subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                            DispatchQueue.main.async {
                                
                                self.itemTableView.reloadData()
                            }
                            
                        }
                        
                    }
                    
                    
                    
                    
                }else{
                    
                    /////////////////////////////////
                    particularSelectedItem.count = itemCount
                    subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[row] = particularSelectedItem
                    
                    print("count changed == \(subServicesThenItemList)")
                    
                    //check if the count is fetched is zero or not if zero remove the item from the array else fe
                    
                    //etche the
                    if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                    }){
                        var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                        if itemCount == 0{
                            
                            self.chosenItemArray.remove(at: rowInChosenItemArray)
                            self.updateSummaryView()
                            
                        }else{
                            
                            particularItemInChosenArray.count = itemCount
                            self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                            self.updateSummaryView()
                        }
                        
                    }else{
                        
                        self.chosenItemArray.append(particularSelectedItem)
                        self.updateSummaryView()
                        
                    }
                    
                }
                
                //show hide of the summary view:
                //If the item count is not zero show the summary. view as soon as the item count is zero then hide the summary view after the item is removed from the array
                if chosenItemArray.count != 0{
                    
                    summaryView.isHidden = false
                    
                }else{
                    
                    summaryView.isHidden = true
                }
                
                
                
                //update the total purchase label to current itemcount * the pack amount
                
                //case is positive:  (First time insert )If on searching the item is not found insert it into summaries item array and increase its count to zero
                //                    If already present change the count to the current count
                //
                //case is negative: (1 is the already availab -make it zero and remoe the item from the array)
                
                
            }
            
        }
        

        
    }
    
    
    func updateSummaryView(){
        
        mainPurchaseCountLbl.text = "\(self.chosenItemArray.count)"
        
        if chosenItemArray.count != 0 {
            
            var total = 0
            for itm in self.chosenItemArray{
                
                total = total + (itm.packArray.first?.rate as! NSString).integerValue*itm.count
                
                
            }
            totalSumOfPurchasedLbl.text = "$\(total)"
        }
        
        
    }
    

    @IBOutlet weak var summaryView: UIView!
    
    @IBOutlet weak var itemTableView: UITableView!
    
    
    var subServicesThenItemList : [subServicesThenItem] = [subServicesThenItem]()
    var addOnArray : [Item] = [Item]()
    
    //Summary view
    @IBOutlet weak var mainPurchaseCountLbl: UILabel!
    
    @IBOutlet weak var totalSumOfPurchasedLbl: UILabel!
    
    
    @IBOutlet weak var summaryBtn: UIButton!
    
    @IBOutlet weak var segmentedControl: ScrollableSegmentedControl!
    
    var chosenItemArray: [Item] = [Item]()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        summaryBtn.backgroundColor = .clear
        summaryBtn.layer.cornerRadius = 15
        summaryBtn.layer.borderWidth = 1
        summaryBtn.layer.borderColor = UIColor.white.cgColor
        
        itemTableView.register(UINib.init(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        itemTableView.rowHeight = UITableView.automaticDimension
        itemTableView.estimatedRowHeight = 300
        segmentedControl.segmentStyle = .textOnly
        var i = 0
        for subServ in self.subServicesThenItemList{
            
            segmentedControl.insertSegment(withTitle: subServ.subName, at: i)
            
            i += i
            
        }
        
        if addOnArray.count != 0{
            
            segmentedControl.insertSegment(withTitle: "Add Ons", at: self.subServicesThenItemList.count)
        }
        
        //segmentedControl.insertSegment(withTitle: "dsfdf", at: 0)
        //segmentedControl.insertSegment(withTitle: "yjhjh", at: 1)
        //segmentedControl.insertSegment(withTitle: "dshjfdf", at: 2)
        //segmentedControl.insertSegment(withTitle: "dshjfdf", at: 3)
        //segmentedControl.insertSegment(withTitle: "dsfdf", at: 0)
        //segmentedControl.insertSegment(withTitle: "dsfdf", at: 0)
        segmentedControl.underlineSelected = true
        
        segmentedControl.addTarget(self, action: #selector(self.segmentSelected(sender:)), for: .valueChanged)
        
        // change some colors
        segmentedControl.segmentContentColor = UIColor.gray
        segmentedControl.selectedSegmentContentColor = UIColor.white
        segmentedControl.backgroundColor = UIColor.black
        segmentedControl.tintColor = UIColor.white
        
        // Turn off all segments been fixed/equal width.
        // The width of each segment would be based on the text length and font size.
        //segmentedControl.underlineHeight = 3.0
        
        segmentedControl.underlineSelected = true
        segmentedControl.selectedSegmentIndex = 0
        //fixedWidthSwitch.isOn = false
        segmentedControl.fixedSegmentWidth = true
        itemTableView.reloadData()
        self.navigationItem.title = "VIPXY"
    }
    
    
    
    
    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
        print("Segment at index \(sender.selectedSegmentIndex)  selected")
        //itemArray = subServicesThenItemList[sender.selectedSegmentIndex].itemArray
        segmentedControl.selectedSegmentIndex = sender.selectedSegmentIndex
        itemTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return itemArray.count
        if segmentedControl.selectedSegmentIndex != self.subServicesThenItemList.count{
            
            return subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray.count
            
        }else{
            return addOnArray.count
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
        if segmentedControl.selectedSegmentIndex != self.subServicesThenItemList.count{
            
            let item = subServicesThenItemList[segmentedControl.selectedSegmentIndex].itemArray[indexPath.row]
            itemCell.model = ItemCellModel(item)
        }else{
            
            let item = addOnArray[indexPath.row]
            itemCell.model = ItemCellModel(item)
        }

        itemCell.purchaseDelegate = self
        
    
        itemCell.selectionStyle = .none
        return itemCell
    }
    
    
    @IBAction func summaryBtnClicked(_ sender: Any) {
        
        print("all the chosen items == \(self.chosenItemArray)")
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let summary = storyBoard.instantiateViewController(withIdentifier: "SummaryVc") as! SummaryVc
        summary.chosenItemArray = self.chosenItemArray
        self.navigationController?.pushViewController(summary, animated: true)
        
        
    }
    
    
    
}

struct Summary{
    var mainPurchaseCount:Int
    var totalSumOfPurchasedItems:Float
    var itemsAdded:[Item]
    
//    init(itemsArray:[Item], purchaseCount:Int, totalSum:Float) {
//        
//        mainPurchaseCount = purchaseCount
//        totalSumOfPurchasedItems = totalSum
//        itemsAdded = itemsArray
//    }
    
}
