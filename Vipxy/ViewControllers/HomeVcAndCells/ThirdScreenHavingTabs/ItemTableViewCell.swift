//
//  ItemTableViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 26/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var packDescriptionLbl: UILabel!
    
    @IBOutlet weak var countDisplayBtn: UIButton!
    
    @IBOutlet weak var plusBtn: UIButton!
    
    @IBOutlet weak var minusBtn: UIButton!
    
    @IBOutlet weak var AddCoverBtn: UIButton!
    
    
    @IBOutlet weak var cellTotalAmountLbl: UILabel!
    

    weak var purchaseDelegate: changeThePurchaseCountDelegate?

    var originalCount = 0
    
    
    var model:ItemCellModel!{
        
        didSet{
            
            nameLabel.text = model.itemName
            descriptionLabel.text = model.itemDescription
            if let imgStr = model.itemImageUrl{
                
                itemImageView.sd_setShowActivityIndicatorView(true)
                itemImageView.sd_setIndicatorStyle(.gray)
                itemImageView.sd_setImage(with: URL(string: model.itemImageUrl!)) { (image, error, cache, urs) in
                    
                    
                }
                
            }else{
                
                itemImageView.image = nil
            }

            originalCount = model.count
            countDisplayBtn.setTitle("\(model.count)", for: .normal)
            
            if model.count == 0{
                
                AddCoverBtn.isHidden = false
                
            }else{
                
                AddCoverBtn.isHidden = true
            }
            
            
            var str = ""
            var i = 0
            for pck in model!.packArray{
                
                if i == (model.packArray.count - 1){
                    
                    str = str + "$\(pck.rate) for \(pck.min) min"
                }else{
                    
                    str = str + "$\(pck.rate) for \(pck.min) min\n"
                }
                
                i = i+1
            }
            packDescriptionLbl.text = str
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    @IBAction func plusBtnClicked(_ sender: Any) {
        
        print("plus button clicked")
        //if model.count == 0{
            
            AddCoverBtn.isHidden = true
            originalCount = originalCount + 1
            countDisplayBtn.setTitle("\(originalCount)", for: .normal)
            
        //}
        
        purchaseDelegate?.changePurchaseCount(itemId: model.itemId!, itemCount: originalCount)
        
        
    }
    
    @IBAction func minusBtnClicked(_ sender: Any) {
        
        print("minus btn clicked")
        
            originalCount = originalCount - 1
            if originalCount > 0{
                AddCoverBtn.isHidden = true
                countDisplayBtn.setTitle("\(originalCount)", for: .normal)
            }else{
                AddCoverBtn.isHidden = false
                countDisplayBtn.setTitle("\(originalCount)", for: .normal)
            }
        purchaseDelegate?.changePurchaseCount(itemId: model.itemId!, itemCount: originalCount)
    }
}

class ItemCellModel{
    let itemId:String?
    let itemName:String?
    let itemDescription:String?
    let hasDiffPack:String?
    let itemImageUrl:String?
    let packArray:[Pack]
    let type:String?
    let count:Int
    init(_ item: Item) {
        itemId = item.itemId
        itemName = item.itemName
        itemDescription = item.itemDescription
        hasDiffPack = item.hasDiffPack
        itemImageUrl = item.itemImage
        packArray = item.packArray
        type = item.type
        count = item.count
    }
    
    
}
protocol changeThePurchaseCountDelegate: class{
    
    func changePurchaseCount(itemId:String, itemCount:Int )
    
}
