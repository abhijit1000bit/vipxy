//
//  AlertForMultiplePacksVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 29/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class AlertForMultiplePacksVc: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GenderSelect {

    
 
    
    @IBOutlet weak var packsCollectionView: UICollectionView!
    
    @IBOutlet weak var maleBtn: UIButton!
    
    
    @IBOutlet weak var femaleBtn: UIButton!
    
    @IBOutlet weak var eitherBtn: UIButton!
    
    weak var purchaseDelegate: oneAmongMultiplePackSelectedDelegate?
    
    var packsArrayReceived:[Pack] = [Pack]()
    var itemId = ""
    var selectedGender = ""
    weak var genderSelectDelegate: GenderSelect?
    var countReceived = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        packsCollectionView.register(UINib.init(nibName: "PackSelectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PackSelectionCollectionViewCell")

        genderSelectDelegate = self
        
    }
    func setGender(gender: String) {
        
        self.selectedGender = gender
        print("slected gender == \(self.selectedGender)")
        
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        

        if let row = self.packsArrayReceived.firstIndex(where: {$0.selected == 1}) {
            
            
            if self.selectedGender == ""{
                
                self.showAlertView(title: "", message: "Please select preference")
            }else{
                
                let particularSelectedCell = self.packsArrayReceived[row]
                purchaseDelegate?.returnBackSelectedPack(itemId: itemId, packId: particularSelectedCell.packId, genderSelected: self.selectedGender, countReceived:countReceived)
                self.dismiss(animated: true, completion: nil)
            }
            

            
        }else{
            
            self.showAlertView(title: "", message: "please select a pack")
        }
        
        //self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func backgroundDismissBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func maleBtnClicked(_ sender: Any) {
        
        maleBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
        femaleBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)
        eitherBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)

        genderSelectDelegate?.setGender(gender: "M")
    }
    
    
    @IBAction func femaleBtnClicked(_ sender: Any) {
        
        maleBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)
        femaleBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
        eitherBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)

        
        genderSelectDelegate?.setGender(gender: "F")
    }
    
    @IBAction func eitherBtnClicked(_ sender: Any) {
        
        maleBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)
        femaleBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)
        eitherBtn.setImage(UIImage(named: "radioSelected"), for: .normal)

        genderSelectDelegate?.setGender(gender: "E")
    }
    
    
    
    // MARK: UICOLLECTION VIEW DELEGATE DATASOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return packsArrayReceived.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PackSelectionCollectionViewCell", for: indexPath) as! PackSelectionCollectionViewCell
        
        let pac = packsArrayReceived[indexPath.row]
        
        if pac.selected == 1 {
            
            cell.packSelectionBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
            
        }else{
            
            cell.packSelectionBtn.setImage(UIImage(named: "radioUnSelected"), for: .normal)
        }
        
        cell.packSelectionBtn.setTitle("$\(pac.rate) for \(pac.min) min", for: .normal)
//        cell.packSelectionBtn.removeTarget(self, action: #selector(self.selectUnselectButtonPressedForPackSelection), for: .touchUpInside)
//        cell.packSelectionBtn.tag = indexPath.row
//        cell.packSelectionBtn.addTarget(self, action: #selector(self.selectUnselectButtonPressedForPackSelection), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("button at \(indexPath.row) selected")

        //var i = 0
        

        for i in 0..<self.packsArrayReceived.count{
            var particularSelectedCell = self.packsArrayReceived[i]
            particularSelectedCell.selected = 0
            self.packsArrayReceived[i] = particularSelectedCell
            
        }
        print("made zero = \(self.packsArrayReceived)")
        
        if let row = self.packsArrayReceived.firstIndex(where: {$0.packId == self.packsArrayReceived[indexPath.row].packId}) {
            
            var particularSelectedCell = self.packsArrayReceived[row]
            particularSelectedCell.selected = 1
            self.packsArrayReceived[row] = particularSelectedCell
            
        }
        

        DispatchQueue.main.async {
            
            self.packsCollectionView.reloadData()
        }
        
        print("made one = \(self.packsArrayReceived)")
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:122,height:40)
    }
    
}


protocol GenderSelect:class {
    
    func setGender(gender:String)
}
