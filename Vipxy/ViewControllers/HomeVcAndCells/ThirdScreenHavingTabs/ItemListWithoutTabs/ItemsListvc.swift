//
//  ItemsListvc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 26/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class ItemsListvc: BaseViewController, UITableViewDataSource, UITableViewDelegate, changeThePurchaseCountDelegate, oneAmongMultiplePackSelectedDelegate {
    
    
    func returnBackSelectedPack(itemId: String, packId: String, genderSelected: String, countReceived: Int) {
        
        if let row = self.itemList.firstIndex(where: {$0.itemId == itemId}) {
            var particularSelectedItem = itemList[row]
            particularSelectedItem.count = countReceived
            itemList[row] = particularSelectedItem
            
            if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
            }){
                var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
 
                    //if particularSelectedItem.count < countReceived{
                        //plus clicked
                         print("original string = \(particularSelectedItem.multiplePackEncoding)")
                        particularSelectedItem.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                        particularSelectedItem.count = countReceived
                        itemList[row] = particularSelectedItem
                        print("new string = \(particularSelectedItem.multiplePackEncoding)")
                        
                        particularItemInChosenArray.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                        particularItemInChosenArray.count = countReceived
                        self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                    //}
                self.updateSummaryView()
      
                
            }else{
                
                
                particularSelectedItem.multiplePackEncoding = "\(packId),\(genderSelected)X"
                particularSelectedItem.count = countReceived
                itemList[row] = particularSelectedItem
                self.chosenItemArray.append(particularSelectedItem)
                print("multiple pack iutem added = \(particularSelectedItem)")
                self.updateSummaryView()
            }
            
//            if particularSelectedItem.count > countReceived{
//                //minus clicked
//
//
//
//            }
//
//            if particularSelectedItem.count < countReceived{
//                //plus ckicked
//
//                //new entry
//                if particularSelectedItem.count == 0{
//                    //insert new item
//                    //insert selected string
//                    self.chosenItemArray.append(particularSelectedItem)
//                    particularSelectedItem.multiplePackEncoding = "\(packId)\(genderSelected)X"
//                    itemList[row] = particularSelectedItem
//
//                }else{
//                    //modify the count
//                    //modify the selected string array
//
//                }
//
//            }
            
            if chosenItemArray.count != 0{
                
                summaryView.isHidden = false
                
            }else{
                
                summaryView.isHidden = true
            }
            
            DispatchQueue.main.async {
                
                self.itemsListTableView.reloadData()
            }
        }
        
    }
    
    
    

    

    func changePurchaseCount(itemId: String, itemCount: Int) {
        if let row = self.itemList.firstIndex(where: {$0.itemId == itemId}) {
            var particularSelectedItem = itemList[row]
            
            
            //If has multiple packs then do this as given below else follow what is normal i.e change the count and calculate the amount
            //MULTIPLE PACKS IN alertview display all the available packs , on chosing a pack append \(packid),\(M)X to
            //multiplePackSelectedString i.e packid and the male, female or either
            //\(packid),\(M)X\(packid),\(F)X\(packid),\(M)X\(packid),\(E)X == 23FX,24EX,24MX,23MX
            
            //Check for a minnus click, compare new count with the original if new is less than original then, remove the last
            //appended string from the components separatedby X array
            //Then crate a new string from components joined by the array
            //Then update the multiple pack encoding string
            
            if particularSelectedItem.hasDiffPack == "Yes"{
                
                //minus clicked
                if particularSelectedItem.count > itemCount{
                    
                    //check if itemCount  == 0
                    if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                    }){
                        var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                        if itemCount == 0 {
                            
                            particularSelectedItem.multiplePackEncoding = ""
                            particularSelectedItem.count = itemCount
                            itemList[row] = particularSelectedItem
                            self.chosenItemArray.remove(at: rowInChosenItemArray)
                            self.updateSummaryView()
                        }else{
                            
                            print("original string = \(particularSelectedItem.multiplePackEncoding)")
                            
                            var compAray = particularSelectedItem.multiplePackEncoding.components(separatedBy: "X")
                            
                            print("comp array original = \(compAray)")
                            
                            var last = compAray.removeLast()
                            
                            print("new comp array = \(compAray)")
                            
                            var secodLast = compAray.removeLast()
                            print("new comp array finally = \(compAray)")
                            var strinRep = compAray.joined(separator: "X")
                            strinRep.append("X")
                            print("new string = \(strinRep)")
                            particularSelectedItem.multiplePackEncoding = strinRep
                            
                            particularSelectedItem.count = itemCount
                            itemList[row] = particularSelectedItem
                            particularItemInChosenArray.multiplePackEncoding = strinRep
                            
                            particularItemInChosenArray.count = itemCount
                            
                            self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                            self.updateSummaryView()
                        }
                        
                    }
                
                    
                }else{
                    
                    //self.showMultiplePacksAlert(packArray: particularSelectedItem.packArray, itemId:itemId)
                    //Show an alert view controller and then sheow three radio buttons....
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertForMultiplePacksVc") as! AlertForMultiplePacksVc
                    myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    //myAlert.mainMessage = title
                    //myAlert.subMessage = message
                    myAlert.packsArrayReceived = particularSelectedItem.packArray
                    myAlert.itemId = itemId
                    myAlert.purchaseDelegate = self
                    myAlert.countReceived = itemCount
                    self.present(myAlert, animated: true, completion: nil)
                    
                    let originalCount = particularSelectedItem.count
                    if originalCount == 0{
                        particularSelectedItem.count = 0
                        itemList[row] = particularSelectedItem
                        DispatchQueue.main.async {
                            
                            self.itemsListTableView.reloadData()
                        }
                        
                    }
                    
                }
                

                
                
            }else{
                
                particularSelectedItem.count = itemCount
                itemList[row] = particularSelectedItem
                
                if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                }){
                    var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                    if itemCount == 0{
                        
                        self.chosenItemArray.remove(at: rowInChosenItemArray)
                        self.updateSummaryView()
                        
                    }else{
                        
                        particularItemInChosenArray.count = itemCount
                        self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                        self.updateSummaryView()
                    }
                    
                }else{
                    
                    self.chosenItemArray.append(particularSelectedItem)
                    self.updateSummaryView()
                    
                }
                
            }
            

            
            //show hide of the summary view:
            //If the item count is not zero show the summary. view as soon as the item count is zero then hide the summary view after the item is removed from the array
            if chosenItemArray.count != 0{
                
                summaryView.isHidden = false
                
            }else{
                
                summaryView.isHidden = true
            }
            
            
        }
    }
    
    
    // MARK: UPDATE SUMMARY VIEW
    
    
    func updateSummaryView(){
        
        mainPurchaseCountLbl.text = "\(self.chosenItemArray.count)"
        
        if chosenItemArray.count != 0 {
            
            var total = 0
            for itm in self.chosenItemArray{
                
                if itm.hasDiffPack == "Yes"{
                    
                    var compAray = itm.multiplePackEncoding.components(separatedBy: "X")
                    
                    
                    var newCompArray = compAray.removeLast()
                    print("array of inserted string = \(compAray)")
                    
                    for selPack in compAray{
                        
                        var separatePackIdAndPref = selPack.components(separatedBy: ",")
                        print("array of inserted string = \(separatePackIdAndPref)")
                        let packIdS = separatePackIdAndPref[0]
                        let pref = separatePackIdAndPref[1]
                        if let rowInChosenPackArray = itm.packArray.firstIndex(where: { $0.packId == packIdS
                        }){
                            
                            let chosenPack = itm.packArray[rowInChosenPackArray]
                            total = total + (chosenPack.rate as! NSString).integerValue
                            
                        }
                        
                    }
                    
                }else{
                    
                    total = total + (itm.packArray.first?.rate as! NSString).integerValue*itm.count
                    
                }
                
                
                
                
            }
            totalSumOfPurchasedLbl.text = "$\(total)"
        }
        
        
    }
    

    @IBOutlet weak var summaryView: UIView!
    
    @IBOutlet weak var itemsListTableView: UITableView!
    
    @IBOutlet weak var totalSumOfPurchasedLbl: UILabel!
    
    @IBOutlet weak var mainPurchaseCountLbl: UILabel!
    
    var itemList:[Item] = [Item]()
    var chosenItemArray: [Item] = [Item]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemsListTableView.register(UINib.init(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        self.navigationItem.title = "VIPXY"
    }
    
    
    //MARK: UITABLEVIEW DATASOURCE AND DELEGATE METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
        
        let item = itemList[indexPath.row]
        itemCell.model = ItemCellModel(item)
        itemCell.purchaseDelegate = self
       // itemCell.nameLabel.text = item.itemName
       // itemCell.descriptionLabel.text = item.itemDescription
       // itemCell.itemImageView.sd_setShowActivityIndicatorView(true)
       // itemCell.itemImageView.sd_setIndicatorStyle(.gray)
       // itemCell.itemImageView.sd_setImage(with: URL(string: item.itemImage)) { (image, error, cache, urs) in
            
            
        //}
       // var str = ""
       // var i = 0
       // for pck in item.packArray{
            
        //    if i == (item.packArray.count - 1){
                
       //         str = str + "$\(pck.rate) for \(pck.min) min"
       //     }else{
                
        //        str = str + "$\(pck.rate) for \(pck.min) min\n"
        //    }
   
        //    i = i+1
        //}
        //itemCell.packDescriptionLbl.text = str
        itemCell.selectionStyle = .none
        
        return itemCell
    }
    
    
    
    @IBAction func summaryBtnClicked(_ sender: Any) {
        
        
        print("all the chosen items == \(self.chosenItemArray)")
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let summary = storyBoard.instantiateViewController(withIdentifier: "SummaryVc") as! SummaryVc
        summary.chosenItemArray = self.chosenItemArray
        self.navigationController?.pushViewController(summary, animated: true)
        
    }
    
    
    
}
protocol oneAmongMultiplePackSelectedDelegate: class{
    
    func returnBackSelectedPack(itemId:String, packId:String, genderSelected:String, countReceived:Int )
    
}
