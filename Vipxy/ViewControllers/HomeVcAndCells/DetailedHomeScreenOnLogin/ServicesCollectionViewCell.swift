//
//  ServicesCollectionViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 25/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ServicesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
  
    
    @IBOutlet weak var cellNameLabel: UILabel!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        
    }

    
    
    
}
