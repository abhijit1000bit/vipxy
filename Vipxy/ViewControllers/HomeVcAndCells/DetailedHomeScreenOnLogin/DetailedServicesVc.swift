//
//  DetailedServicesVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 25/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class DetailedServicesVc : BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate{

    let collectionViewHeaderFooterReuseIdentifier = "MyHeaderFooterClass1"
    var msid = ""
    let cellId = "CellId"
    var servicesArray : [[Service]] = [[Service]]()
    var basicArray: [[String:Any]] = [[:]]
    var subServicesThenItemsArray: [[subServicesThenItem]] = [[subServicesThenItem]]()
    
    var itemsArray: [[Item]] = [[Item]]()
    var serviceName = ""
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        collectionView.register(UINib.init(nibName: "ServicesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServicesCollectionViewCell")
        
        collectionView.register(MyHeaderFooterClass.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier)
        
        collectionView.register(Cell.self, forCellWithReuseIdentifier: cellId)
        self.navigationItem.title = "VIPXY"
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (section == 0) ? 1 : self.servicesArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1  {
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServicesCollectionViewCell", for: indexPath) as! ServicesCollectionViewCell
            
            let service = self.servicesArray[indexPath.row]
            
            cell.cellNameLabel.text = service.first?.sName
            
            cell.cellImageView.sd_setImage(with: URL(string: service.first!.sImage)) { (image, error, cache, urs) in
                
            }
            
            
            
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath) as! Cell
            //        cell.backgroundColor = .blue;
            //self.passBannerdatDelegate?.passBannerData(bannerArray: upcomingBannerArray)
            //cell.upcomingBannerArray = self.upcomingBannerArray
//            cell.passBannerData(bannerArray: upcomingBannerArray)
//            cell.launchuploadDelegate = self
            return cell;
            
        }

        //return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.section == 0 {
            
            
            return CGSize(width: 375, height: 180)
            
        }else{
            //                let padding: CGFloat =  25
            //                let collectionViewSize = collectionView.frame.size.width - padding
            
            return CGSize(width: 115, height: 154)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 57.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var v : UICollectionReusableView! = nil
        if kind == UICollectionView.elementKindSectionHeader {
            v = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: self.collectionViewHeaderFooterReuseIdentifier, for: indexPath)
            v.backgroundColor = .black

            if indexPath.section == 0 {
                //lab.text = "UPCOMING COMPETITIONS"
                
                if v.subviews.count == 0{
                    v.addSubview(UISearchBar(frame: CGRect(x: 50, y: 10, width: 284, height: 37)))
                    
                }
               let searcHBaar = v.subviews[0] as! UISearchBar
//                txtF.placeholder = "Beauty, salon, health, cleaning etc."
//                txtF.backgroundColor = .white
//                txtf.place
                
                searcHBaar.placeholder = "Beauty, Cleaning, Health, Salon etc."
                searcHBaar.setPlaceholder(textColor: .lightGray)
                
                
                searcHBaar.tintColor = .black
                searcHBaar.backgroundColor = .white
                //searcHBaar.setSearchFieldBackgroundImage(UIImage(named: "signin_username_bg"), for: .normal)
                //searchBar.translatesAutoresizingMaskIntoConstraints = false
                searcHBaar.clipsToBounds = true
                searcHBaar.layer.cornerRadius = 2
                searcHBaar.barTintColor = .black
                //searcHBaar.setNewcolor(color: .yellow)
                searcHBaar.setPlaceholder(textColor: UIColor(red:0.82, green:0.82, blue:0.83, alpha:1.0))
                searcHBaar.setTextField(color: .white)
                searcHBaar.delegate = self
                
            } else {
                if v.subviews.count == 0 {
                    v.addSubview(UILabel(frame:CGRect(x: 60, y: 18, width: collectionView.frame.width, height: 21)))
                }
                let lab = v.subviews[0] as! UILabel
                lab.font = UIFont.boldSystemFont(ofSize: 14)
                lab.textColor = .white
                lab.textAlignment = .left
                var str = self.serviceName.uppercased()
                lab.text = "\(str)"
            }
            //            lab.text = self.sections[indexPath.section].sectionName
            
        }
        return v
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let service = self.servicesArray[indexPath.row]
        let sid = service.first?.sid
        //getSubService(sid: sid!)
        getSUbserv(sid: sid!, subServiceTitle: service.first!.sName)
        
    }
    
    //MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""
        {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        
    }
    
    //func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    // hideSearchBar()
    //}
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        
        searchItem(searchText: searchBar.text!)
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
        
        
        print("keyboard search clicked")
    }
    
    func searchItem(searchText:String){
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            //self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            
            startLoading()
            var request = URLRequest(url: URL(string: "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/search_items")!)
            request.httpMethod = "POST"
            let postString = "search=\(searchText)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil else{
                    
                    DispatchQueue.main.async {
                        self.stopLoading()
                    }
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    
                    
                }else{
                    //parse
                    self.subServiceResponse(data: data)
                }
                DispatchQueue.main.async {
                    self.stopLoading()
                }
                
            }
            task.resume()
            
        }
    }
    
    
    func subServiceResponse(data: Data){
        
        DispatchQueue.main.async {
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                let str = json!["message"] as? String
                let value = json!["message_code"] as? Int
                if (value == 1){
                    let jsonResult = try JSONDecoder().decode(OuterJsonSearchItems.self, from: data)
                    print("final json result JsonItems  == \(jsonResult.list)")
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let itemListScreenVc = storyBoard.instantiateViewController(withIdentifier: "ItemsListvc") as! ItemsListvc
                    
                    itemListScreenVc.itemList = jsonResult.list
                    let backItem = UIBarButtonItem()
                    backItem.title = "Back"
                    self.navigationItem.backBarButtonItem = backItem
                    self.navigationController?.pushViewController(itemListScreenVc, animated: true)
                    
                }else{
                    
                    self.showAlertView(title: "Alert", message: str!)
                }
                
            }catch{
                
                print("Error with Json: \(error)")
            }
            
        }
        
        
    }
    
    func getSUbserv(sid:String, subServiceTitle:String){
        //self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            //self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            

            startLoading()
            var request = URLRequest(url: URL(string: "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/get_item_with_subservices")!)
            request.httpMethod = "POST"
            let postString = "sid=\(sid)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil else{
                    
                    DispatchQueue.main.async {
                        self.stopLoading()
                    }
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    
                    
                }else{
                    //parse
                    self.subServiceResponse(data: data, subservicTitle: subServiceTitle)
                }
                DispatchQueue.main.async {
                    self.stopLoading()
                }

            }
            task.resume()
            
        }

    }
    
    func subServiceResponse(data: Data, subservicTitle:String){
        
        DispatchQueue.main.async {
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                let str = json!["message"] as? String
                let value = json!["message_code"] as? Int
                if (value == 1){
                    
                    let sDetails = json!["s_details"] as? [String:Any]
                    print("sdetails received = \(sDetails!)")
                    let hassubServiceThenItem = sDetails!["has_subservices_then_item"] as! String
                    if hassubServiceThenItem == "Yes"{
                        let jsonResult = try JSONDecoder().decode(OuterJsonSubserviceThenItems.self, from: data)
                        print("final json result SubserviceThenItems == \(jsonResult.list)")
                        
                        let hasAddOn = sDetails!["has_addon"] as! String
                        if hasAddOn == "Yes"{
                            
                            let jsonResultAddon = try JSONDecoder().decode(OuterJsonSubserviceThenItemsWithAddOn.self, from: data)
                            
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let detailedServicesScreenVc = storyBoard.instantiateViewController(withIdentifier: "SubSevicesThenItemsVc") as! SubSevicesThenItemsVc
                            detailedServicesScreenVc.subServicesThenItemList = jsonResultAddon.list
                            detailedServicesScreenVc.addOnArray = jsonResultAddon.addOnList
                            let backItem = UIBarButtonItem()
                            backItem.title = subservicTitle
                            self.navigationItem.backBarButtonItem = backItem
                            self.navigationController?.pushViewController(detailedServicesScreenVc, animated: true)
                            
                        }else{
                            
                            
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let detailedServicesScreenVc = storyBoard.instantiateViewController(withIdentifier: "SubSevicesThenItemsVc") as! SubSevicesThenItemsVc
                            detailedServicesScreenVc.subServicesThenItemList = jsonResult.list
                            let backItem = UIBarButtonItem()
                            backItem.title = subservicTitle
                            self.navigationItem.backBarButtonItem = backItem
                            self.navigationController?.pushViewController(detailedServicesScreenVc, animated: true)
                            
                        }
                        


                        
                        
                    }else{
                        let jsonResult = try JSONDecoder().decode(OuterJsonItems.self, from: data)
                        print("final json result JsonItems  == \(jsonResult.list)")
                        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                        let itemListScreenVc = storyBoard.instantiateViewController(withIdentifier: "ItemsListvc") as! ItemsListvc
                        
                        itemListScreenVc.itemList = jsonResult.list
                        let backItem = UIBarButtonItem()
                        backItem.title = subservicTitle
                        self.navigationItem.backBarButtonItem = backItem
                        self.navigationController?.pushViewController(itemListScreenVc, animated: true)
                        
                    }
                    
                    //self.showAlertView(title: "Alert", message: str!)
                }else{
                    
                    self.showAlertView(title: "Alert", message: str!)
                }
                
                
            }catch{
                print("Error with Json: \(error)")
            }
        }
        
    }
    
    func getSubService(sid:String){
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().getItemsWithSubServices(sid: sid) { (status, message, ListArray, hasSubServicesThenItem, subServiceName) in
                
                
                if status == true{
                    self.stopLoading()
                    
                    if hasSubServicesThenItem == "Yes"{
                        
                        print("subServicesThenItemsArray received = \(ListArray)")
                        self.basicArray = ListArray as? [[String:Any]] ?? [[:]]
                        self.subServicesThenItemsArray = [[subServicesThenItem]]()
                        
                        for subStItem in self.basicArray{
                            
                            //let subSerThenItem = subServicesThenItem(json: subStItem)
                           // self.subServicesThenItemsArray.append([subSerThenItem])
                            
                        }
                        
                        do{
                            
                            //let jsonResult = try JSONDecoder().decode([subServicesThenItem], from: ListArray)
                            
                        }catch{
                            
                            
                        }
                        
                        
                        print("subservices Then Items Modeled Array = \(self.subServicesThenItemsArray)")
                        
                    }else{
                        
                        print("ItemsArray received = \(ListArray)")
                        self.basicArray = ListArray as? [[String:Any]] ?? [[:]]
                        self.itemsArray = [[Item]]()
                        
                        for item in self.basicArray{
                            
                            //let itemParsed = Item(json: item)
                            //self.itemsArray.append([itemParsed])
                            
                        }
                        
                        print("items modeled array = \(self.itemsArray)")
                        
                    }
                    
                    
                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
            }
            
        }
        
        
    }
    
}



class MyHeaderFooterClass: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.purple
        
        // Customize here
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}


class Cell: UICollectionViewCell{
    
    let cellId = "CellId";
    //lazy var fmOne = CGRect(x: 0, y: 0, width: self.frame.width, height: 180)
    //lazy var lbl = UILabel(frame: fmOne)
    let welcomeTextLabel: UILabel = {
        let label = UILabel()
        label.text = "WELCOME TO VIPXY\n\nHome services for a number of issues like cleaning, health and wellness support services, beauty salon services including skin and hair care and even massage and spas! Everything will be brought to the user in the comfort of home."
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpviews(){
        
      
        
        addSubview(welcomeTextLabel)
        welcomeTextLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        welcomeTextLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        welcomeTextLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        welcomeTextLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10).isActive = true
        //welcomeTextLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        //welcomeTextLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 10).isActive = true
        welcomeTextLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
    
    
}
extension UISearchBar {
    
    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func set(textColor: UIColor) { if let textField = getTextField() { textField.textColor = textColor } }
    func setPlaceholder(textColor: UIColor) { getTextField()?.setPlaceholder(textColor: textColor) }
    func setClearButton(color: UIColor) { getTextField()?.setClearButton(color: color) }
    
    func setTextField(color: UIColor) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color.cgColor
            textField.layer.cornerRadius = 2
            textField.font = UIFont.systemFont(ofSize: 11.0)
        case .prominent, .default: textField.backgroundColor = color
            textField.font = UIFont.systemFont(ofSize: 11.0)
        @unknown default: break
        }
    }
    
    func setSearchImage(color: UIColor) {
        guard let imageView = getTextField()?.leftView as? UIImageView else { return }
        imageView.tintColor = color
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
    }
}

private extension UITextField {
    
    private class Label: UILabel {
        private var _textColor = UIColor.lightGray
        override var textColor: UIColor! {
            set { super.textColor = _textColor }
            get { return _textColor }
        }
        
        init(label: UILabel, textColor: UIColor = .lightGray) {
            _textColor = textColor
            super.init(frame: label.frame)
            self.text = label.text
            self.font = UIFont.systemFont(ofSize: 11.0)
        }
        
        required init?(coder: NSCoder) { super.init(coder: coder) }
    }
    
    
    private class ClearButtonImage {
        static private var _image: UIImage?
        static private var semaphore = DispatchSemaphore(value: 1)
        static func getImage(closure: @escaping (UIImage?)->()) {
            DispatchQueue.global(qos: .userInteractive).async {
                semaphore.wait()
                DispatchQueue.main.async {
                    if let image = _image { closure(image); semaphore.signal(); return }
                    guard let window = UIApplication.shared.windows.first else { semaphore.signal(); return }
                    let searchBar = UISearchBar(frame: CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 44))
                    window.rootViewController?.view.addSubview(searchBar)
                    searchBar.text = "txt"
                    searchBar.layoutIfNeeded()
                    _image = searchBar.getTextField()?.getClearButton()?.image(for: .normal)
                    closure(_image)
                    searchBar.removeFromSuperview()
                    semaphore.signal()
                }
            }
        }
    }
    
    func setClearButton(color: UIColor) {
        ClearButtonImage.getImage { [weak self] image in
            guard   let image = image,
                let button = self?.getClearButton() else { return }
            button.imageView?.tintColor = color
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    var placeholderLabel: UILabel? { return value(forKey: "placeholderLabel") as? UILabel }
    
    func setPlaceholder(textColor: UIColor) {
        guard let placeholderLabel = placeholderLabel else { return }
        let label = Label(label: placeholderLabel, textColor: textColor)
        setValue(label, forKey: "placeholderLabel")
    }
    
    func getClearButton() -> UIButton? { return value(forKey: "clearButton") as? UIButton }
}
