//
//  SideMenuViewController.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 02/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class SideMenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate {
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellS = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cellS.labelMenu.text = menuArray[indexPath.row]
        let imageName = imageIconsArray[indexPath.row]
        cellS.iconImageView.image = UIImage(named: imageName)
        
        cellS.selectionStyle = .none
        return cellS
        
//        let cellIdentifier = "SideMenuCell"
//
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SideMenuCell  else {
//            fatalError("The dequeued cell is not an instance of SideMenuCell.")
//        }
//        let title = menuArray[indexPath.row]
//        cell.labelMenu.text = menuArray[indexPath.row]
//        cell.preservesSuperviewLayoutMargins = false
//        cell.separatorInset = UIEdgeInsets.zero
//        cell.layoutMargins = UIEdgeInsets.zero
//        cell.selectionStyle = .none
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
//            if Utility.getUserType() == "1" {
//                let VC:UserHomeVC! = storyboard!.instantiateViewController(withIdentifier: "UserHomeVC") as? UserHomeVC
//                self.navigationController?.pushViewController(VC, animated: false)
//            } else if Utility.getUserType() == "2" {
//                //                let VC:PastorHomeVC! = storyboard!.instantiateViewController(withIdentifier: "PastorHomeVC") as? PastorHomeVC
//                //                self.navigationController?.pushViewController(VC, animated: false)
//            }
            break
        case 1:
            //            if Utility.getUserType() == "1" {
            //                let VC:FindChurchVC! = storyboard!.instantiateViewController(withIdentifier: "FindChurchVC") as? FindChurchVC
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            } else if Utility.getUserType() == "2" {
            ////                let VC:PastorHomeVC! = storyboard!.instantiateViewController(withIdentifier: "PastorHomeVC") as? PastorHomeVC
            ////                self.navigationController?.pushViewController(VC, animated: false)
            //            }
            break
        case 2:
            
            //            if Utility.getUserType() == "1" {
            //                self.shareOnShocialMedia()
            //            } else if Utility.getUserType() == "2" {
            //                let VC:UserMessagesVC! = storyboard!.instantiateViewController(withIdentifier: "UserMessagesVC") as? UserMessagesVC
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            }
            break
        case 3:
            //            if Utility.getUserType() == "1" {
            //                let VC:UserMessagesVC! = storyboard!.instantiateViewController(withIdentifier: "UserMessagesVC") as? UserMessagesVC
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            } else if Utility.getUserType() == "2" {
            //                let VC:PastorAccountVC! = storyboard!.instantiateViewController(withIdentifier: "PastorAccountVC") as? PastorAccountVC
            //                VC.delegate = self
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            }
            break
        case 4:
            //            if Utility.getUserType() == "1" {
            //                let VC:AccountVC! = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as? AccountVC
            //                VC.delegate = self
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            } else if Utility.getUserType() == "2" {
            //                let VC:PastorSettingsVC! = storyboard!.instantiateViewController(withIdentifier: "PastorSettingsVC") as? PastorSettingsVC
            //                self.navigationController?.pushViewController(VC, animated: false)
            //            }
            //logout()
            break
        case 5:
//            if Utility.getUserType() == "1" {
//                let VC:UserSettingsVC! = self.storyboard?.instantiateViewController(withIdentifier: "UserSettingsVC") as? UserSettingsVC
//                //            VC.fromVC = "FromSideMenu"
//                self.navigationController?.pushViewController(VC, animated: false)
//            } else if Utility.getUserType() == "2" {
//                let VC:UserHelpVC! = self.storyboard?.instantiateViewController(withIdentifier: "UserHelpVC") as? UserHelpVC
//                self.navigationController?.pushViewController(VC, animated: false)
//            }
            
            break
        case 6:
//            if Utility.getUserType() == "1" {
//                let VC:UserHelpVC! = self.storyboard?.instantiateViewController(withIdentifier: "UserHelpVC") as? UserHelpVC
//                self.navigationController?.pushViewController(VC, animated: false)
//            } else if Utility.getUserType() == "2" {
//                logout()
//            }
//
            break
        case 7:
            logout()
            break
        default: break
        }
        
        DispatchQueue.main.async {
            //            if indexPath.row != 2 {
            //                self.dismiss(animated: true, completion: nil)
            //            } else
            
            //            if Utility.getUserType() == "1" {
            if indexPath.row != 7 {
                self.dismiss(animated: true, completion: nil)
            }
            //            }
            
            
        }
        
    }
    
    func logout() {
        
        DispatchQueue.main.async {
            let objAlertController = UIAlertController(title: "LOGOUT", message: "Do you really want to LOGOUT?", preferredStyle: .alert)
            
            let objAction = UIAlertAction(title: "CONFIRM", style: .destructive, handler:
            {Void in
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                UserDefaults.standard.set(nil, forKey: "loginStatus")
                UserDefaults.standard.synchronize()
                print("LoginStatus:",Utility.getLoginStatus())
                self.navigationController?.pushViewController(LoginViewController, animated: true)
                
            })
            objAlertController.addAction(objAction)
            
            let cancelAction = UIAlertAction(title: "CANCEL", style: .default, handler:
            {Void in
                
            })
            objAlertController.addAction(cancelAction)
            
            self.present(objAlertController, animated: true, completion: nil)
        }
        
    }
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var locationImageView: UIImageView!
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    var menuArray: [String] = []
    var imageIconsArray: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
                menuArray = ["Home", "Contact Us","Payment History","My Jobs","Notification","Setting","Account","Logout"]
        imageIconsArray = ["menu_home","menu_contact_us","menu_payment","menu_job","menu_notification","menu_setting","menu_profile","Composite Path"]
          tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
        
        nameLabel.text = Utility.getUserName()
        var otherImg = UIImage(named: "other_icon")?.withRenderingMode(.alwaysTemplate)
        locationImageView.tintColor = .lightGray
        locationImageView.image = otherImg
    }
    
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if ((self.navigationController) != nil)
        {
            self.navigationController!.isNavigationBarHidden = true
        }
    }
    
    @IBAction func crossButtonClicked(_ sender: Any) {
        
        
        
    }
    
    
    
    
    @IBAction func editProfileBtnClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
        self.navigationController?.pushViewController(LoginViewController, animated: true)
        
    }
    
}
