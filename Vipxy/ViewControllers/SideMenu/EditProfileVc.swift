//
//  EditProfileVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import Foundation
import UIKit

class EditProfileVc: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    var type = ""
    var text = ""
    
    @IBOutlet weak var subscriptLbl: UILabel!
    
    @IBOutlet weak var currentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subscriptLbl.text = type
        self.currentTextField.text = text
        
        
        
    }
    
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        
        
        if type == "Email"{
            if (!isValidEmail(testStr: currentTextField.text!))
            {
                //self.showAlertView(title: "Alert", message: "Please enter valid email.")
                self.showAlertView(title: "Alert", message: "Please enter a valid email.")
                
                
            }else{
                
                
                self.startLoading()
                if(!ReachabilitySwift.isConnectedToNetwork()){
                    self.stopLoading()
                    showAlertView(title: "Error", message: "Please check your internet connection")
                    return
                }else{
                    
                    APIController().updateTheField(field: "Email", updatedValue: currentTextField.text!) { (status, message) in
                        
                        if status == true{
                            self.stopLoading()

                            self.showAlertView(title: "Alert", message: message)

                        }else{
                            
                            self.stopLoading()
                            self.showAlertView(title: "Failure", message: message)

                        }
                        
                    }
       
                }
                
            }
            
            
        }else if type == "Contact Number"{
            
            if ((currentTextField.text?.trimmingCharacters(in: .whitespaces).count)!<7)
            {
                //self.showAlertView(title: "Alert", message: "phone number should have mininmum 10 characters.")
                self.showAlertView(title: "Alert", message: "Mobile number should have mininmum 10 characters.")
            }else{
                
                
                self.startLoading()
                if(!ReachabilitySwift.isConnectedToNetwork()){
                    self.stopLoading()
                    showAlertView(title: "Error", message: "Please check your internet connection")
                    return
                }else{
                    
                    APIController().updateTheField(field: "Contact Number", updatedValue: currentTextField.text!) { (status, message) in
                        
                        if status == true{
                            self.stopLoading()
                            
                            self.showAlertView(title: "Alert", message: message)
                            
                        }else{
                            
                            self.stopLoading()
                            self.showAlertView(title: "Failure", message: message)
                            
                        }
                        
                    }
                    
                }
                
            }
            
            
            
        }else if type == "Change Password"{
            
            
            if ((currentTextField.text?.trimmingCharacters(in: .whitespaces).count)!<7)
            {
                //self.showAlertView(title: "Alert", message: "Password should have mininmum 7 characters.")
                self.showAlertView(title: "Alert", message: "Password should have mininmum 7 characters.")
             
            }else{
                
                self.startLoading()
                if(!ReachabilitySwift.isConnectedToNetwork()){
                    self.stopLoading()
                    showAlertView(title: "Error", message: "Please check your internet connection")
                    return
                }else{
                    
                    APIController().updateTheField(field: "Change Password", updatedValue: currentTextField.text!) { (status, message) in
                        
                        if status == true{
                            self.stopLoading()
                            
                            self.showAlertView(title: "Alert", message: message)
                            
                        }else{
                            
                            self.stopLoading()
                            self.showAlertView(title: "Failure", message: message)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
    }
    
    
}

