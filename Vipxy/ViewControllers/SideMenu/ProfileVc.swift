//
//  ProfileVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 15/01/20.
//  Copyright © 2020 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ProfileVc: BaseViewController ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var emailTf: UITextField!
    
    @IBOutlet weak var locationTf: UITextField!
    
    @IBOutlet weak var contactNumTf: UITextField!
    
   
    @IBOutlet weak var passwdTf: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getProfileDetails()
    }
    
    
    func getProfileDetails(){
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            
            
            
            APIController().getProfile(uid: Utility.getuserId()) { (status, message, email, contact, passwd, profileUrl) in
            
                if status == true{
                    self.stopLoading()
                    
                    self.emailTf.text = email
                    self.contactNumTf.text = contact
                    self.passwdTf.text = passwd
                    self.locationTf.text = "-"
                    
                    
                    self.profileImgView.sd_setImage(with: URL(string: profileUrl), placeholderImage: UIImage(named: "camera_task_details"))
                    
                }else{
                    self.stopLoading()
                    
                    self.showAlertView(title: "Failure", message: message)

                }
                
            }
            
            
        }
        
    }
    
    @IBAction func editEmailClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let editProfVc = storyBoard.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        
        editProfVc.text = emailTf.text!
        
        editProfVc.type = "Email"
        //dateTimeForOrderVc.chosenItemArray = self.chosenItemArray
        //dateTimeForOrderVc.deliveryAddress = self.fetchedAddressArray[indexPath.row]
        
        self.navigationController!.pushViewController(editProfVc, animated: false)
        
        
    }
    
    @IBAction func addLocationClicked(_ sender: Any) {
        
        
    }
    
    
    @IBAction func editContactClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let editProfVc = storyBoard.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        
        editProfVc.text = contactNumTf.text!
        
        editProfVc.type = "Contact Number"
        //dateTimeForOrderVc.chosenItemArray = self.chosenItemArray
        //dateTimeForOrderVc.deliveryAddress = self.fetchedAddressArray[indexPath.row]
        
        self.navigationController!.pushViewController(editProfVc, animated: false)
        
    }
    
    @IBAction func changePasswordClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let editProfVc = storyBoard.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        
        editProfVc.text = passwdTf.text!
        
        editProfVc.type = "Change Password"
        //dateTimeForOrderVc.chosenItemArray = self.chosenItemArray
        //dateTimeForOrderVc.deliveryAddress = self.fetchedAddressArray[indexPath.row]
        
        self.navigationController!.pushViewController(editProfVc, animated: false)
        
    }
    
    
    @IBAction func changePhotoClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Take Picture", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.takePicture(iscamera:true)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.takePicture(iscamera:false)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func takePicture(iscamera:Bool)
    {
        startLoading()
        let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        
        //self.imagePickerController.delegate = self
        
        if iscamera
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
        }
        else
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        }
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        stopLoading()

        var im = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        if let ed = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            im = ed
        }
        
        self.profileImgView.image = im
        
        
        self.dismiss(animated: true, completion: nil)
        updateProfilePic()
    }
    
    func updateProfilePic(){
        
        self.startLoading()
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().updateUserDetails(userId: Utility.getuserId(), image: self.profileImgView.image!) { (status, message) in
                
                if status == true{
                    self.stopLoading()
                    
                    self.showAlertView(title: "Alert", message: message)
                    
                    
                }else{
                    
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
            }
            
        }
        
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        stopLoading()
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


