//
//  LoginRegisterVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 21/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class LoginRegisterVc: BaseViewController, VerifyOtpAndLoginDelegate {
    
    func otpVerifiedNowLogin(blogsArray: [[Blog]], chosenItemArray: [Item]) {
        
        if chosenItemArray.count != 0 {
            
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let serviceAddrVc = storyBoard.instantiateViewController(withIdentifier: "ChooseServiceAddressVc") as! ChooseServiceAddressVc
            serviceAddrVc.chosenItemArray = chosenItemArray
            //homeVc.blogsArray = self.blogsArray
            self.navigationController?.pushViewController(serviceAddrVc, animated: true)
            
            
        }else{
            
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVc = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVc") as! HomeScreenVc
            homeVc.blogsArray = blogsArray
            self.navigationController?.pushViewController(homeVc, animated: true)
        }
        
        
    }
    

    
    
    
    var basicArray: [[String:Any]] = [[:]]
    
    var blogsArray : [[Blog]] = [[Blog]]()
    
    var chosenItemArray : [Item] = [Item]()
    
    //SIGN UP
    @IBOutlet weak var fullNameTf: UITextField!
    
    @IBOutlet weak var emailidTf: UITextField!
    
    @IBOutlet weak var mobileNumberTf: UITextField!
    
    @IBOutlet weak var passwordTf: UITextField!
    
    //LOGIN
    
    @IBOutlet weak var loginEmailTf: UITextField!
    
    @IBOutlet weak var loginPasswordTf: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = .black
         //navigationController?.setNavigationBarHidden(false, animated: animated)
        fullNameTf.text = nil
        emailidTf.text = nil
        mobileNumberTf.text = nil
        passwordTf.text = nil
        loginEmailTf.text = nil
        loginPasswordTf.text = nil
        
    }

    
    @IBAction func signUpBtnClicked(_ sender: Any) {
        
        
    }
    
    
    @IBAction func getOtpAndVerifyYourAccountBtnClicked(_ sender: Any) {
        
//        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        let homeVc = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVc") as! HomeScreenVc
//
//        self.navigationController?.pushViewController(homeVc, animated: true)
        
        if checkForEmpty(){
            
            self.startLoading()
            
            if(!ReachabilitySwift.isConnectedToNetwork()){
                self.stopLoading()
                showCustomisedAlert(title: "Error", message: "Please check your internet connection")
                return
            }else{
                APIController().signUpUser(fullName: fullNameTf.text!, email: emailidTf.text!, mobile: mobileNumberTf.text!, password: passwordTf.text!) { (status, message, userId) in
                    
                    
                    if status == true{
                        
                        self.stopLoading()
                        Utility.setUserId(userId)
                        Utility.setLoginStatus()
                        //self.showOtpAlert(title: message)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let myAlert = storyboard.instantiateViewController(withIdentifier: "OtpAlertVc") as! OtpAlertVc
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        myAlert.chosenItemArray = self.chosenItemArray
                        //myAlert.mainMessage = title
                        //myAlert.subMessage = message
                        myAlert.otpVerifiedAndLoginDelegaet = self
                        
                        myAlert.alertTitle = message
                        self.present(myAlert, animated: true, completion: nil)
                        
                        
                    }else{
                        
                        self.stopLoading()
                        self.showCustomisedAlert(title: "Failure", message: message)
                        
                    }
                    

                }

            }
            //getBlogs()
        }
    }
 
    @IBAction func forgotPasswordBtnClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let resetPasswrd = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordVc") as! ResetPasswordVc
        
        self.navigationController?.pushViewController(resetPasswrd, animated: true)
        
        
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        if checkForEmptyLoginDetails(){
            
            self.startLoading()
            
            if(!ReachabilitySwift.isConnectedToNetwork()){
                self.stopLoading()
                showAlertView(title: "Error", message: "Please check your internet connection")
                return
            }else{
                
                APIController().login(phoneOrEmail: loginEmailTf.text!, password: loginPasswordTf.text!) { (status, message, userData) in
                    
                    
                    if status ==  true{
                        
                        
                        Utility.setUserId(userData["uid"] as! String)
                        Utility.setUserName(userData["full_name"] as! String)
                        Utility.setEmail(userData["email"] as! String)
                        Utility.setMobile(userData["mobile"] as! String)
                        Utility.setStatus(userData["status"] as! String)
                        Utility.setLoginStatus()
                        
                        DispatchQueue.main.async {
                            self.stopLoading()
                            self.getBlogs()
                            
                            //let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "HomeScreenVc") as! HomeScreenVc
                            
                            //let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                            //self.present(navController, animated:true, completion: nil)
                        }
                        
                    }else{
                        
                        
                        self.stopLoading()
                        //self.showAlertView(title: "Failure", message: message)
                        self.showCustomisedAlert(title: "Failure", message: message)
                    }
                    
                    
                }
                
                
                
                
            }
            
            
        }
        
        
    }
    
    func checkForEmpty() -> Bool {
        
        if (fullNameTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter full name.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter full name.")
            return false
        }
        else if (emailidTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter email.")
            return false
        }
        else if (!isValidEmail(testStr: emailidTf.text!))
        {
            //self.showAlertView(title: "Alert", message: "Please enter valid email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter a valid email.")
            return false
            
        }else if (mobileNumberTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            //self.showAlertView(title: "Alert", message: "Please enter mobile number.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter mobile number.")
            return false
            
        }
        else if ((mobileNumberTf.text?.trimmingCharacters(in: .whitespaces).count)!<7)
        {
            //self.showAlertView(title: "Alert", message: "phone number should have mininmum 10 characters.")
            self.showCustomisedAlert(title: "Alert", message: "Mobile number should have mininmum 10 characters.")
            return false
        }
        else if (passwordTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter password.")
             self.showCustomisedAlert(title: "Alert", message: "Please enter password.")
            return false
        }
        else if ((passwordTf.text?.trimmingCharacters(in: .whitespaces).count)!<7)
        {
            //self.showAlertView(title: "Alert", message: "Password should have mininmum 7 characters.")
            self.showCustomisedAlert(title: "Alert", message: "Password should have mininmum 7 characters.")
            return false
        }
        else
        {
            return true
        }
        
    }

    
    
    func checkForEmptyLoginDetails() -> Bool {
        
        if (loginEmailTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter email.")
            return false
        }
        else if (!isValidEmail(testStr: loginEmailTf.text!))
        {
            //self.showAlertView(title: "Alert", message: "Please enter valid email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter valid email.")
            return false
        }
        else if (loginPasswordTf.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter password.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter password.")
            return false
        }
        else
        {
            return true
        }
        
    }
    
    
    func getBlogs(){
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            
            return
        }else{
            
            APIController().getBlogs { (status, message, blogsAray) in
                
                
                if status ==  true
                {
                    self.stopLoading()
                    
                    print("received blogs array = \(blogsAray)")
                    
                    self.basicArray = blogsAray as? [[String:Any]] ?? [[:]]
                    self.blogsArray = [[Blog]]()
                    
                    for blog in self.basicArray{
                        
                        let bloog = Blog(json: blog)
                        self.blogsArray.append([bloog])
                        
                    }
                    
                    print("blogs arry = \(self.blogsArray)")
                    
                    if self.chosenItemArray.count != 0{
                        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                        let serviceAddrVc = storyBoard.instantiateViewController(withIdentifier: "ChooseServiceAddressVc") as! ChooseServiceAddressVc
                        //homeVc.blogsArray = self.blogsArray
                        serviceAddrVc.chosenItemArray = self.chosenItemArray
                        self.navigationController?.pushViewController(serviceAddrVc, animated: true)
                        
                    }else{
                        
                        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                        let homeVc = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVc") as! HomeScreenVc
                        homeVc.blogsArray = self.blogsArray
                        self.navigationController?.pushViewController(homeVc, animated: true)
                    }
                    
                    

                    
                    
                    
                }else{
                    
                    self.stopLoading()
                    //self.showAlertView(title: "Failure", message: message)
                    self.showCustomisedAlert(title: "Failure", message: message)
                    
                }
                
                
            }
            
            
            
        }
        
        
    }
    
    
}
protocol VerifyOtpAndLoginDelegate: class {
    
    func otpVerifiedNowLogin(blogsArray:[[Blog]],  chosenItemArray:[Item])
    
}
