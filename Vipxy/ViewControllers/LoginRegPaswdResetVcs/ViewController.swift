//
//  ViewController.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 21/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    var basicArray: [[String:Any]] = [[:]]
    
    var blogsArray : [[Blog]] = [[Blog]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
    }

    @IBAction func getStartedBtnClicked(_ sender: Any) {
        
        getBlogs()
        //let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        //let timeLineMusicVideoVc = storyBoard.instantiateViewController(withIdentifier: "LoginRegisterVc") as! LoginRegisterVc
        
        //self.navigationController?.pushViewController(timeLineMusicVideoVc, animated: true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getBlogs(){
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            
            return
        }else{
            
            APIController().getBlogs { (status, message, blogsAray) in
                
                
                if status ==  true
                {
                    self.stopLoading()
                    
                    print("received blogs array = \(blogsAray)")
                    
                    self.basicArray = blogsAray as? [[String:Any]] ?? [[:]]
                    self.blogsArray = [[Blog]]()
                    
                    for blog in self.basicArray{
                        
                        let bloog = Blog(json: blog)
                        self.blogsArray.append([bloog])
                        
                    }
                    //UserDefaults.standard.set(true, forKey: "loginStatus")
                    //UserDefaults.standard.removeObject(forKey: "loginStatus")
                    //UserDefaults.standard.synchronize()
                    print("blogs arry = \(self.blogsArray)")
                    
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let homeVc = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVc") as! HomeScreenVc
                    homeVc.blogsArray = self.blogsArray
                    self.navigationController?.pushViewController(homeVc, animated: true)
                    
                    
                    
                }else{
                    
                    self.stopLoading()
                    //self.showAlertView(title: "Failure", message: message)
                    self.showCustomisedAlert(title: "Failure", message: message)
                    
                }
                
                
            }
            
            
            
        }
        
        
    }


}

