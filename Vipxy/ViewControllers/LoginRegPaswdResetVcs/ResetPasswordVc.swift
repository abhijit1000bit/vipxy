//
//  ResetPasswordVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 21/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class ResetPasswordVc: BaseViewController{
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func enterClicked(_ sender: Any) {
        
        if checkForEmptyLoginDetails(){
            
            self.startLoading()
            
            if(!ReachabilitySwift.isConnectedToNetwork()){
                self.stopLoading()
                showAlertView(title: "Error", message: "Please check your internet connection")
                return
            }else{
                
                APIController().getResetPasswordLink(email: emailTextField.text!) { (status, message) in
                    
                    if status == true{
                        self.stopLoading()
                        self.showCustomisedAlert(title: "Alert", message: message)
                        
                    }else{
                        self.stopLoading()
                        self.showCustomisedAlert(title: "Alert", message: message)
                        
                    }
                }
                
                
            }
            
            
        }
        
//        //Test code for alaunching the alert view
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
//        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        myAlert.mainMessage = "sdfsdfdsf"
//        myAlert.subMessage = "SDFDSF"
//        
//        self.present(myAlert, animated: true, completion: nil)

    }
    
    func checkForEmptyLoginDetails() -> Bool {
        
        if (emailTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            //self.showAlertView(title: "Alert", message: "Please enter email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter email.")
            return false
        }
        else if (!isValidEmail(testStr: emailTextField.text!))
        {
            //self.showAlertView(title: "Alert", message: "Please enter valid email.")
            self.showCustomisedAlert(title: "Alert", message: "Please enter valid email.")
            return false
        }
        else
        {
            return true
        }
        
    }
}
