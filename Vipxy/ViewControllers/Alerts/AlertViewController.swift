//
//  AlertViewController.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 21/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class AlertViewController: UIViewController {
    
    @IBOutlet weak var mainMessageLbl: UILabel!
    
    @IBOutlet weak var subMessageLabel: UILabel!
    
    var mainMessage = ""
    var subMessage = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        mainMessageLbl.text = mainMessage
        subMessageLabel.text = subMessage
        
    }
    
    
    @IBAction func clickHereClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

        
    }
    
    
    @IBAction func backGroundDismissBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
