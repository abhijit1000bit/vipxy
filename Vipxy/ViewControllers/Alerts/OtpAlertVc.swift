//
//  OtpAlertVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 27/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class OtpAlertVc: BaseViewController {

    @IBOutlet weak var otpView: SROTPView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var basicArray: [[String:Any]] = [[:]]
    
    var blogsArray : [[Blog]] = [[Blog]]()
    var chosenItemArray: [Item] = [Item]()
    var alertTitle = ""
    var otpEntered = ""
    var userId = ""
    
    weak var otpVerifiedAndLoginDelegaet:VerifyOtpAndLoginDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = alertTitle
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        otpView.otpTextFieldsCount = 6
        otpView.otpTextFieldActiveBorderColor = UIColor.blue
        otpView.otpTextFieldDefaultBorderColor = UIColor.red
        otpView.otpTextFieldFontColor = UIColor.black
        otpView.cursorColor = UIColor.black
        otpView.otpTextFieldBorderWidth = 2
        otpView.otpEnteredString = { pin in
            print("The entered pin is \(pin)")
            self.otpEntered = pin
        }
    }
    
    override func viewDidLayoutSubviews() {
        otpView.initializeUI()
    }

    
    
    @IBAction func backgroundDismissBtnClicked(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func verifyOtpClicked(_ sender: Any) {
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showCustomisedAlert(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().verifyuser(userid: Utility.getuserId(), otpReceived: otpEntered) { (status, message) in
                
                if status ==  true{
                    
                    self.getBlogs()
                    
                    
                    
                }else{
                    self.stopLoading()
                    self.showCustomisedAlert(title: "alert", message: message)
                    
                }
                
            }
        }
    }
    
    
    
    func getBlogs(){
        //self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            APIController().getBlogs { (status, message, blogsAray) in
                
                
                if status ==  true
                {
                    self.stopLoading()
                    
                    print("received blogs array = \(blogsAray)")
                    
                    self.basicArray = blogsAray as? [[String:Any]] ?? [[:]]
                    self.blogsArray = [[Blog]]()
                    
                    for blog in self.basicArray{
                        
                        let bloog = Blog(json: blog)
                        self.blogsArray.append([bloog])
                        
                    }
                    
                    print("blogs arry = \(self.blogsArray)")
                    
                    self.otpVerifiedAndLoginDelegaet?.otpVerifiedNowLogin(blogsArray: self.blogsArray, chosenItemArray: self.chosenItemArray)

                    

                    
                     self.dismiss(animated: true, completion: nil)
                    
                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
}
