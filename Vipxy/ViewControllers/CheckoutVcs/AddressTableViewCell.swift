//
//  AddressTableViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 04/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class AddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tagNameLbl: UILabel!
    
    @IBOutlet weak var addresslbl: UILabel!
    
    @IBOutlet weak var tagImgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
