//
//  AddNewAddressVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 02/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class AddNewAddressVc: BaseViewController, GMSMapViewDelegate, CLLocationManagerDelegate, GMSAutocompleteViewControllerDelegate, popThevc{
    
    
    func popViewContrlr() {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //weak var delegateAddressChange: reflectCurrentlocationDelegate?
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
    
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
    }
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    var marker : GMSMarker!
    var markerEndPoint : GMSMarker!
    var locationManager : CLLocationManager!
    let geocoder = GMSGeocoder()
    var addressString:String = ""
    var currentLatitude:Double?
    var currentLongitude:Double?
    
    func googleMapSettings() {
        marker = GMSMarker()
        marker.map = mapView
        
        markerEndPoint = GMSMarker()
        
        marker.map = nil //To hide the marker
        self.mapView.settings.myLocationButton = true //To show/hide current location button
        self.mapView.isMyLocationEnabled = true //To show/hide blue dot
        
        //        let theImageView = UIImageView(image:UIImage(named:"MovingCarIcon"))
        //        theImageView.image = theImageView.image!.withRenderingMode(.alwaysTemplate)
        //        theImageView.tintColor = UIColor.blue.withAlphaComponent(0.55)
        
        //creating a marker view
        //        self.marker.iconView = theImageView
        //        self.marker.map = self.mapView
        
        startSignificantChangeUpdates()
    }
    
    func startSignificantChangeUpdates(){
        
        if self.locationManager == nil{
            
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest//ForNavigation
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.activityType = .automotiveNavigation
            self.locationManager.startUpdatingLocation()
            
            if self.locationManager.location?.coordinate.latitude != nil{
                
                let position = CLLocationCoordinate2D(latitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!)
                
                mapView.camera = GMSCameraPosition.camera(withTarget: position, zoom: 20)
                
                let geocoder = GMSGeocoder()
                geocoder.reverseGeocodeCoordinate(position) { (response, error) in
                    if error != nil{
                     
                        print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
                        
                        
                    }else{
                        let result = response?.results()?.first
                        let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1
                            
                            
                        }
                        print("address 3 :",address as? String)
                        
                    }
                }
            }
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idleAt")
        
        returnPositionOfMapView(mapView: mapView)
    }
    
    func returnPositionOfMapView(mapView:GMSMapView){
        
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let position = CLLocationCoordinate2DMake(latitude, longitude)
        geocoder.reverseGeocodeCoordinate(position) { (response, error) in
            if error != nil{
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else{
                let result = response?.results()?.first
                if let address = result?.lines?.reduce("", { $0 == "" ? $1 : $0 + ", " + $1}){
                    self.addressString = address
                    self.currentLatitude = latitude
                    self.currentLongitude = longitude
                    print("address 1 :",address as! String)
                    //self.delegateAddressChange?.upadateTheCurrentaddress(currentAddress: address)
                    self.cardViewController.upadateTheCurrentaddress(currentAddress: address)
                    
                    self.cardViewController.popDelegate = self
                }
            }
            
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations called")
        currentLatitude = manager.location!.coordinate.latitude
        currentLongitude = manager.location!.coordinate.longitude
        
        let position = CLLocationCoordinate2D(latitude: manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude)
        mapView.camera = GMSCameraPosition.camera(withTarget: position, zoom: 18)
        marker.position = position
        
        geocoder.reverseGeocodeCoordinate(position) { response, error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else{
                let result = response?.results()?.first
                if let address = result?.lines?.reduce("", { $0 == "" ? $1 : $0 + ", " + $1
                }){
                    self.addressString = address
                    self.currentLatitude = manager.location!.coordinate.latitude
                    self.currentLongitude = manager.location!.coordinate.longitude
                    
                    print("address 2 :",address)
                    self.cardViewController.upadateTheCurrentaddress(currentAddress: address)
                    
                    //self.cardViewController.popDelegate = self
                }
                
            }
            
        }
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse{
            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
        if  CLLocationManager.locationServicesEnabled(){
            switch(CLLocationManager.authorizationStatus()){
            case .notDetermined, .restricted, .denied:
                print("No access")
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
            }
            
        }else{
            print("Location services are not enabled")
        }
         print("Error while updating location " + error.localizedDescription)
    }
    
    
    
    
    
    
    
    // Enum for card states
    enum CardState {
        case collapsed
        case expanded
    }
    
    // Variable determines the next state of the card expressing that the card starts and collapased
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    
    // Variable for card view controller
    var cardViewController:CardViewController!
    
    // Variable for effects visual effect view
    var visualEffectView:UIVisualEffectView!
    
    // Starting and end card heights will be determined later
    var endCardHeight:CGFloat = 0
    var startCardHeight:CGFloat = 0
    
    // Current visible state of the card
    var cardVisible = false
    
    // Empty property animator array
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        setupCard()
        googleMapSettings()
        
        
    }

    func setupCard() {
        // Setup starting and ending card height
        endCardHeight = self.view.frame.height * 0.51
        startCardHeight = self.view.frame.height * 0.2
        
        // Add Visual Effects View
        //visualEffectView = UIVisualEffectView()
        //visualEffectView.frame = self.view.frame
        //self.view.addSubview(visualEffectView)
        
        // Add CardViewController xib to the bottom of the screen, clipping bounds so that the corners can be rounded
        cardViewController = CardViewController(nibName:"CardViewController", bundle:nil)
        self.view.addSubview(cardViewController.view)
        cardViewController.view.frame = CGRect(x: 0, y: self.view.frame.height - startCardHeight, width: self.view.bounds.width, height: endCardHeight)
        cardViewController.view.clipsToBounds = true
        
        // Add tap and pan recognizers
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleCardTap(recognzier:)))
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleCardPan(recognizer:)))
        
        cardViewController.handleArea.addGestureRecognizer(tapGestureRecognizer)
        cardViewController.handleArea.addGestureRecognizer(panGestureRecognizer)
    }
    
    // Handle tap gesture recognizer
    @objc
    func handleCardTap(recognzier:UITapGestureRecognizer) {
        switch recognzier.state {
        // Animate card when tap finishes
        case .ended:
            animateTransitionIfNeeded(state: nextState, duration: 0.9)
        default:
            break
        }
    }
    
    // Handle pan gesture recognizer
    @objc
    func handleCardPan (recognizer:UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            // Start animation if pan begins
            startInteractiveTransition(state: nextState, duration: 0.9)
            
        case .changed:
            // Update the translation according to the percentage completed
            let translation = recognizer.translation(in: self.cardViewController.handleArea)
            var fractionComplete = translation.y / endCardHeight
            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
            updateInteractiveTransition(fractionCompleted: fractionComplete)
        case .ended:
            // End animation when pan ends
            continueInteractiveTransition()
        default:
            break
        }
    }
    
    
    
    // Animate transistion function
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        // Check if frame animator is empty
        if runningAnimations.isEmpty {
            // Create a UIViewPropertyAnimator depending on the state of the popover view
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    // If expanding set popover y to the ending height and blur background
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.endCardHeight
                    //self.visualEffectView.effect = UIBlurEffect(style: .dark)
                    
                case .collapsed:
                    // If collapsed set popover y to the starting height and remove background blur
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.startCardHeight
                    
                    //self.visualEffectView.effect = nil
                }
            }
            
            // Complete animation frame
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                //HIGH ALERT:: CRASH
                self.runningAnimations.removeAll()
            }
            
            // Start animation
            frameAnimator.startAnimation()
            
            // Append animation to running animations
            runningAnimations.append(frameAnimator)
            
            // Create UIViewPropertyAnimator to round the popover view corners depending on the state of the popover
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    // If the view is expanded set the corner radius to 30
                    self.cardViewController.view.layer.cornerRadius = 30
                    
                case .collapsed:
                    // If the view is collapsed set the corner radius to 0
                    self.cardViewController.view.layer.cornerRadius = 0
                }
            }
            
            // Start the corner radius animation
            cornerRadiusAnimator.startAnimation()
            
            // Append animation to running animations
            runningAnimations.append(cornerRadiusAnimator)
            
        }
    }
    
    // Function to start interactive animations when view is dragged
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        
        // If animation is empty start new animation
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        
        // For each animation in runningAnimations
        for animator in runningAnimations {
            // Pause animation and update the progress to the fraction complete percentage
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    // Funtion to update transition when view is dragged
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        // For each animation in runningAnimations
        for animator in runningAnimations {
            // Update the fraction complete value to the current progress
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    // Function to continue an interactive transisiton
    func continueInteractiveTransition (){
        // For each animation in runningAnimations
        for animator in runningAnimations {
            // Continue the animation forwards or backwards
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
}
