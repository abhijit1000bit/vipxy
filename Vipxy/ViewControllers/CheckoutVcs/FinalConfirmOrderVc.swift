//
//  FinalConfirmOrderVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 11/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class FinalConfirmOrderVc: BaseViewController, PayPalPaymentDelegate {
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        //resultText = ""
        //successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            //self.resultText = completedPayment.description
            self.showSuccess()
            self.sendCompletedpaymentserver(completedPayment: completedPayment.confirmation as NSDictionary)
        })
    }
    
    
    func sendCompletedpaymentserver(completedPayment : NSDictionary)
    {
        let dicResponse: AnyObject? = completedPayment.object(forKey: "response") as AnyObject
        
        let id = dicResponse!.object(forKey:"id")
        //let state = dicResponse!.object(forKey:"state")
        let subscr_date = dicResponse!.object(forKey:"create_time")
        self.startLoading()
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            let jsonString = JSONStringify(value: parameters as AnyObject)
            print(jsonString)
            APIController().placeTheFinalOrder(parameters: jsonString, uid: Utility.getuserId(), addressId: addressId, serviceDate: serviceDate, serviceTime: serviceTime, paymentType: "Paypal", paymentTxnId: id as! String, msId: Utility.getMainServicesId()) { (status, message) in
                
                if status == true{
                    
                    self.stopLoading()
                    //self.showAlertView(title: "Alert", message: message)
                    let alertController = UIAlertController( title: "Alert", message: message, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { Void in
                        
                        if let viewController = self.navigationController?.viewControllers.first(where: {$0 is HomeScreenVc}) {
                            
                            self.navigationController?.popToViewController(viewController, animated: false)
                        }
                        
                        
                    })
                    
                    //let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    //you can add custom actions as well
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                }
            }
            
            
        }
        
    }
    
    func showSuccess() {
        //successView.isHidden = false
        //successView.alpha = 1.0
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationDelay(2.0)
        //successView.alpha = 0.0
        UIView.commitAnimations()
    }
    
    @IBOutlet weak var addressOneTf: UITextField!
    
    @IBOutlet weak var addressTwoTf: UITextField!
    
    @IBOutlet weak var zipCodeTf: UITextField!
    
    
    @IBOutlet weak var serviceDateTf: UITextField!
    
    @IBOutlet weak var serviceTimeTf: UITextField!
    
    
    @IBOutlet weak var additionalNotesTf: UITextField!
    
    
    @IBOutlet weak var totalPayableAmtLbl: UILabel!
    
    
    var placeOrderItemsArray:[placeOrderItem] = [placeOrderItem]()
    var serviceDate = ""
    var serviceTime = ""
    var addressId = ""
    var finalPrice = ""
    var postReqItemArray:[String:AnyObject] = [String:AnyObject]()

    var parameters = [[String:AnyObject]]()

    var deliveryAddress:Address?
    
    var payPalConfig = PayPalConfiguration()
    
    var environment:String = PayPalEnvironmentNoNetwork{
        willSet(newEnvironment){
            if (newEnvironment != environment){
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        addressOneTf.text = "\(deliveryAddress!.landMark) \(deliveryAddress!.houseNumber)"
        
        addressTwoTf.text = "\(deliveryAddress!.location)"
        serviceDateTf.text = serviceDate
        serviceTimeTf.text = serviceTime
        totalPayableAmtLbl.text = "Total Payable:$\(finalPrice)"
        
        for x in placeOrderItemsArray{
            
            var dict = ["itemid":x.itemId as AnyObject,"pckid":x.packId as AnyObject,"count":x.count as AnyObject,"multiplePackEncoding":x.multiplePackEncoding as AnyObject]
            
        
            parameters.append(dict)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        environment = PayPalEnvironmentSandbox
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    func paypalConfiguration(){
        
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "Vipxy"
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .payPal
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        addPaymentDetails()
    }
    
    func addPaymentDetails(){
        let item1 = PayPalItem(name: "Service", withQuantity: 1, withPrice: NSDecimalNumber(string: finalPrice), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Subscribe", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    @IBAction func backgroundCoverBtnTapped(_ sender: Any) {
        
        
        
    }
    
    @IBAction func confirmYourOrderClicked(_ sender: Any) {
        paypalConfiguration()
//        self.startLoading()
//
//        if(!ReachabilitySwift.isConnectedToNetwork()){
//            self.stopLoading()
//            showAlertView(title: "Error", message: "Please check your internet connection")
//            return
//        }else{
//            let jsonString = JSONStringify(value: parameters as AnyObject)
//            print(jsonString)
//            APIController().placeTheFinalOrder(parameters: jsonString, uid: Utility.getuserId(), addressId: addressId, serviceDate: serviceDate, serviceTime: serviceTime, paymentType: "Paypal") { (status, message) in
//
//                if status == true{
//
//                    self.stopLoading()
//                    //self.showAlertView(title: "Alert", message: message)
//                    let alertController = UIAlertController( title: "Alert", message: message, preferredStyle: .alert)
//                    let action = UIAlertAction(title: "OK", style: .default, handler: { Void in
//
//                        if let viewController = self.navigationController?.viewControllers.first(where: {$0 is HomeScreenVc}) {
//
//                            self.navigationController?.popToViewController(viewController, animated: false)
//                        }
//
//
//                    })
//
//                    //let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//                    //you can add custom actions as well
//                    alertController.addAction(action)
//                    self.present(alertController, animated: true, completion: nil)
//
//                }else{
//
//                    self.stopLoading()
//                    self.showAlertView(title: "Failure", message: message)
//                }
//            }
//
//
//        }
        
    }
    
    
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                
                print("error")
                //Access error here
            }
            
        }
        return ""
        
    }
    
}
