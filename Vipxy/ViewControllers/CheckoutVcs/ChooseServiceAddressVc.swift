//
//  ChooseServiceAddressVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 02/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class ChooseServiceAddressVc: BaseViewController, UITableViewDataSource, UITableViewDelegate{

     var chosenItemArray: [Item] = [Item]()
    
    
    @IBOutlet weak var addressTableView: UITableView!
    
    
    var fetchedAddressArray:[Address] = [Address]()
    
    var tenDaysfromNow: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: 10, to: Date(), options: [])!
    }
    
    override func viewDidLoad() {
        
        print("chosen item array in choose address = \(self.chosenItemArray)")
        
        super.viewDidLoad()
    
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action:#selector(self.backToInitial(sender:)))

        addressTableView.rowHeight = UITableView.automaticDimension
        addressTableView.estimatedRowHeight = 300
        
        addressTableView.register(UINib.init(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")
        
        
        fetchAddresses(uid: Utility.getuserId())
        
        
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let day = calendar.component(.weekday, from: date)
        
        print("date == \(date)")
        print("calendar == \(calendar)")
        print("hour == \(hour)")
        print("minutes == \(minutes)")
        print("day == \(day)")
        
        let weekdays = [
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat"
        ]
        
        print("ten days from now = \(tenDaysfromNow)")
        
        var dateArray = [Date]()
        

        var timeArray = [Date]()
        
        
        for i in 1..<11 {
            
           dateArray.append((Calendar.current as NSCalendar).date(byAdding: .day, value: i, to: Date(), options: [])!)
            
            timeArray.append((Calendar.current as NSCalendar).date(byAdding: .minute, value: i*30, to: Date(), options: [])!)
            
        }
        
        print("array of dates == \(dateArray)")
        
        print("array of times == \(timeArray)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchAddresses(uid: Utility.getuserId())
        
    }
    @objc func backToInitial(sender: AnyObject) {
        //self.navigationController?.popToRootViewController(animated: true)
        
        if let viewController = navigationController?.viewControllers.first(where: {$0 is HomeScreenVc}) {
            
            navigationController?.popToViewController(viewController, animated: false)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedAddressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let address = fetchedAddressArray[indexPath.row]
        
        let addressCell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell", for: indexPath) as! AddressTableViewCell
        addressCell.tagNameLbl.text = address.tag
        addressCell.addresslbl.text = "\(address.houseNumber) \(address.location) \(address.landMark)"
        
        switch address.tag {
        case "Home":
            
            addressCell.tagImgView.image = UIImage(named: "home_icon")
            
            break
            
        case "Work":
            
            addressCell.tagImgView.image = UIImage(named: "work__icon")
            break
            
        case "Other":
            
            addressCell.tagImgView.image = UIImage(named: "other_icon")
            break
        default:
            
            addressCell.tagImgView.image = nil
            break
        }
        addressCell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        return addressCell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dateTimeForOrderVc = storyBoard.instantiateViewController(withIdentifier: "SelectDateTimeForOrderVc") as! SelectDateTimeForOrderVc
        dateTimeForOrderVc.chosenItemArray = self.chosenItemArray
        dateTimeForOrderVc.deliveryAddress = self.fetchedAddressArray[indexPath.row]
        
        self.navigationController!.pushViewController(dateTimeForOrderVc, animated: false)
        
        
    }

    
    
    
    
    @IBAction func chooseNewAddressBtnClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let addNewAddrVc = storyBoard.instantiateViewController(withIdentifier: "AddNewAddressVc") as! AddNewAddressVc
        
        self.navigationController?.pushViewController(addNewAddrVc, animated: true)
        
        
    }
    
    
    func fetchAddresses(uid:String){
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            //self.stopLoading()
            showAlertView(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            
            startLoading()
            var request = URLRequest(url: URL(string: "http://tech599.com/tech599.com/johnaks/vipxy/api/Api/get_address")!)
            request.httpMethod = "POST"
            let postString = "uid=\(uid)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil else{
                    
                    DispatchQueue.main.async {
                        self.stopLoading()
                    }
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    
                    
                }else{
                    //parse
                    self.subServiceResponse(data: data)
                }
                DispatchQueue.main.async {
                    self.stopLoading()
                }
                
            }
            task.resume()
            
        }

        
    }
    
    
    func subServiceResponse(data: Data){
        
        DispatchQueue.main.async {
            self.stopLoading()
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                let str = json!["message"] as? String
                let value = json!["message_code"] as? Int
                if (value == 1){
                    let jsonResult = try JSONDecoder().decode(OuterJsonForAddressList.self, from: data)
                    print("final json result JsonItems  == \(jsonResult.list)")
                    self.fetchedAddressArray = jsonResult.list
                    self.addressTableView.reloadData()
                    
                    
                }else{
                     self.showAlertView(title: "Alert", message: str!)
                }
                
                
            }catch{
                print("Error with Json: \(error)")
                
                
            }
            
            
        }
        
    }
}


extension Date{
    
    func dayOfTheWeek() -> String?{
        
        let weekdays = [
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat"
        ]
        
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.weekday, from: date)
        return weekdays[day - 1]
        
        
    }
    
}
