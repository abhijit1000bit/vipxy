//
//  CardViewController.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 05/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import UIKit

class CardViewController: BaseViewController {
    
    func upadateTheCurrentaddress(currentAddress: String) {
        
        
        locationTf.text = currentAddress
    }
    

    @IBOutlet weak var homeBtn: UIButton!
    
    @IBOutlet weak var workBtn: UIButton!
    
    
    @IBOutlet weak var otherBtn: UIButton!
    
    
    @IBOutlet weak var handleArea: UIView!
    
    @IBOutlet weak var locationTf: UITextField!
    
    
    @IBOutlet weak var houseTf: UITextField!
    
    
    @IBOutlet weak var landMarkTf: UITextField!
    
    
    @IBOutlet weak var homeUnderCoverView: UIView!
    
    
    @IBOutlet weak var workUnderCoverView: UIView!
    
    
    @IBOutlet weak var otherUnderCoverView: UIView!
    
    
    weak var popDelegate:popThevc?
    
    var workImg = UIImage(named: "work__icon")?.withRenderingMode(.alwaysTemplate)
    var homeImg = UIImage(named: "home_icon")?.withRenderingMode(.alwaysTemplate)
    var otherImg = UIImage(named: "other_icon")?.withRenderingMode(.alwaysTemplate)
    
    var tag = "Home"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //var workImg = UIImage(named: "work__icon")?.withRenderingMode(.alwaysTemplate)
        //var homeImg = UIImage(named: "home_icon")?.withRenderingMode(.alwaysTemplate)
        //var otherImg = UIImage(named: "other_icon")?.withRenderingMode(.alwaysTemplate)
        
    

        
        homeBtn.tintColor = .black
        homeBtn.setImage(homeImg, for: .normal)
        homeBtn.setTitleColor(.black, for: .normal)
        homeUnderCoverView.backgroundColor = .black
        
        workBtn.tintColor = .lightGray
        workBtn.setImage(workImg, for: .normal)
        workBtn.setTitleColor(.lightGray, for: .normal)
        workUnderCoverView.backgroundColor = .lightGray
        
        otherBtn.tintColor = .lightGray
        otherBtn.setImage(otherImg, for: .normal)
        otherBtn.setTitleColor(.lightGray, for: .normal)
        otherUnderCoverView.backgroundColor = .lightGray
    }


    @IBAction func nextBtnClicked(_ sender: Any) {
        
        
        self.startLoading()
        
        if(!ReachabilitySwift.isConnectedToNetwork()){
            self.stopLoading()
            showCustomisedAlert(title: "Error", message: "Please check your internet connection")
            return
        }else{
            
            var location = ""
            var houseNo = ""
            var landmark = ""
            
            location = locationTf.text ?? ""
            houseNo = houseTf.text ?? ""
            landmark = landMarkTf.text ?? ""
            
            APIController().addNewAddress(uid: Utility.getuserId(), tag: tag, location: location, houseNumber: houseNo, landmark: landmark) { (status, message) in
                
                if status == true{
                    
                    self.stopLoading()
                    DispatchQueue.main.async {
                        let objAlertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
                        
                        let objAction = UIAlertAction(title: "OK", style: .default, handler:
                        {Void in
                            //let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            //let LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            //UserDefaults.standard.set(nil, forKey: "loginStatus")
                            //UserDefaults.standard.synchronize()
                            //print("LoginStatus:",Utility.getLoginStatus())
                            //self.navigationController?.pushViewController(LoginViewController, animated: true)
                            
                            self.popDelegate?.popViewContrlr()
                            
                        })
                        objAlertController.addAction(objAction)
                        
                        
                        self.present(objAlertController, animated: true, completion: nil)
                    }

                    
                    //self.showAlertView(title: "Alert", message: message)
                    
                }else{
                    
                    self.stopLoading()
                    self.showAlertView(title: "Failure", message: message)
                    
                }
                
                
            }

            
        }
        
        
    }
    @IBAction func homeBtnClicked(_ sender: Any) {
        
        homeBtn.tintColor = .black
        homeBtn.setImage(homeImg, for: .normal)
        homeBtn.setTitleColor(.black, for: .normal)
        homeUnderCoverView.backgroundColor = .black

        workBtn.tintColor = .lightGray
        workBtn.setImage(workImg, for: .normal)
        workBtn.setTitleColor(.lightGray, for: .normal)
        workUnderCoverView.backgroundColor = .lightGray

        otherBtn.tintColor = .lightGray
        otherBtn.setImage(otherImg, for: .normal)
        otherBtn.setTitleColor(.lightGray, for: .normal)
        otherUnderCoverView.backgroundColor = .lightGray

        tag = "Home"
        
    }
    
    @IBAction func workBtnClicked(_ sender: Any) {
        
        homeBtn.tintColor = .lightGray
        homeBtn.setImage(homeImg, for: .normal)
        homeBtn.setTitleColor(.lightGray, for: .normal)
        homeUnderCoverView.backgroundColor = .lightGray

        workBtn.tintColor = .black
        workBtn.setImage(workImg, for: .normal)
        workBtn.setTitleColor(.black, for: .normal)
        workUnderCoverView.backgroundColor = .black

        otherBtn.tintColor = .lightGray
        otherBtn.setImage(otherImg, for: .normal)
        otherBtn.setTitleColor(.lightGray, for: .normal)
        otherUnderCoverView.backgroundColor = .lightGray

        tag = "Work"
    }
    
    @IBAction func otherBtnClicked(_ sender: Any) {
        
        homeBtn.tintColor = .lightGray
        homeBtn.setImage(homeImg, for: .normal)
        homeBtn.setTitleColor(.lightGray, for: .normal)
        homeUnderCoverView.backgroundColor = .lightGray

        workBtn.tintColor = .lightGray
        workBtn.setImage(workImg, for: .normal)
        workBtn.setTitleColor(.lightGray, for: .normal)
        workUnderCoverView.backgroundColor = .lightGray

        otherBtn.tintColor = .black
        otherBtn.setImage(otherImg, for: .normal)
        otherBtn.setTitleColor(.black, for: .normal)
        otherUnderCoverView.backgroundColor = .black

        tag = "Other"
    }
    
    
    
    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


protocol popThevc:class {
    
    func popViewContrlr()
}
