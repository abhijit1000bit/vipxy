//
//  SelectDateTimeForOrderVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 05/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class SelectDateTimeForOrderVc: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var selectDateCollectionView: UICollectionView!
    
    @IBOutlet weak var selectTimeCollectionView: UICollectionView!
    
    
    var chosenItemArray: [Item] = [Item]()
    var deliveryAddress:Address?
    var dateArray = [Date]()
    
    var placeOrderItemsArray:[placeOrderItem] = [placeOrderItem]()

    var timeArray = [Date]()
    
    var dateArrayN = [DateTimeSelected]()
    var timeArrayN = [DateTimeSelected]()
    
    var indexSel = [Int]()
    
    let DATETAG = 100
    let TIMETAG = 220
    
    var finalPrice = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("chosen Item Array = \(chosenItemArray)")

        for i in 1..<11 {
            
            //dateArray.append((Calendar.current as NSCalendar).date(byAdding: .day, value: i, to: Date(), options: [])!)
            
            timeArray.append((Calendar.current as NSCalendar).date(byAdding: .minute, value: i*30, to: Date(), options: [])!)
            
            let ds = DateTimeSelected(dateS: (Calendar.current as NSCalendar).date(byAdding: .minute, value: i*30, to: Date(), options: [])!, selValue: 0)
            timeArrayN.append(ds)
            indexSel.append(0)
        }
        
        for i in 0..<10 {
            
            dateArray.append((Calendar.current as NSCalendar).date(byAdding: .day, value: i, to: Date(), options: [])!)
            
            let ds = DateTimeSelected(dateS: (Calendar.current as NSCalendar).date(byAdding: .day, value: i, to: Date(), options: [])!, selValue: 0)
            dateArrayN.append(ds)
        }
        
        
        print("array of dates == \(dateArray)")
        
        print("array of times == \(timeArray)")
        
        selectDateCollectionView.register(UINib.init(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateCollectionViewCell")
        
        
        selectTimeCollectionView.register(UINib.init(nibName: "TimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TimeCollectionViewCell")

    }
    
    
    func calculateTotal(){
        
        //mainPurchaseCountLbl.text = "\(self.chosenItemArray.count)"
        
        if chosenItemArray.count != 0 {
            
            var total = 0
            for itm in self.chosenItemArray{
                
                if itm.hasDiffPack == "Yes"{
                    
                    var compAray = itm.multiplePackEncoding.components(separatedBy: "X")
                    
                    
                    var newCompArray = compAray.removeLast()
                    print("array of inserted string = \(compAray)")
                    
                    for selPack in compAray{
                        
                        var separatePackIdAndPref = selPack.components(separatedBy: ",")
                        print("array of inserted string = \(separatePackIdAndPref)")
                        let packIdS = separatePackIdAndPref[0]
                        let pref = separatePackIdAndPref[1]
                        if let rowInChosenPackArray = itm.packArray.firstIndex(where: { $0.packId == packIdS
                        }){
                            
                            let chosenPack = itm.packArray[rowInChosenPackArray]
                            total = total + (chosenPack.rate as! NSString).integerValue
                            
                        }
                        
                    }
                    
                }else{
                    
                    total = total + (itm.packArray.first?.rate as! NSString).integerValue*itm.count
                    
                }
                
                
                
                
            }
            finalPrice = "\(total)"
            //subTotalLbl.text = "$\(total)"
            //discountLabel.text = "0.0"
            
        }else{
            finalPrice = "0.0"
            //subTotalLbl.text = "$ 0.0"
            //discountLabel.text = "0.0"
            
        }
        
        
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        let filteredDate = self.dateArrayN.filter{ $0.selectedValue == 1
        }
        
        let filteredTime = self.timeArrayN.filter { $0.selectedValue == 1
        }
        
        if filteredDate.count > 0{
            
    
            if filteredTime.count > 0{
                
                
                //Move to final confirm order screen
                print("can proceed")
                
                self.placeOrderItemsArray = [placeOrderItem]()
                calculateTotal()
                
                for i in self.chosenItemArray{
                    
                    if i.multiplePackEncoding != ""{
                        
                        let placeitem = placeOrderItem(itemID: i.itemId, packID: i.packArray.first!.packId, count: "\(i.count)", multiplePackEncoding: String(i.multiplePackEncoding.dropLast()))
                        
                        placeOrderItemsArray.append(placeitem)
                        
                    }else{
                        
                        let placeitem = placeOrderItem(itemID: i.itemId, packID: i.packArray.first!.packId, count: "\(i.count)", multiplePackEncoding:i.multiplePackEncoding)
                        placeOrderItemsArray.append(placeitem)
                        
                    }

                    
                    
                }
                
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let finalOrder = storyBoard.instantiateViewController(withIdentifier: "FinalConfirmOrderVc") as! FinalConfirmOrderVc
                finalOrder.placeOrderItemsArray = self.placeOrderItemsArray
                
                let servDate = getDateInCustmizeFormat("yyyy-MM-dd HH:mm:ss Z", dateString: getStringFromDate(filteredDate.first!.date, inputFormat: "yyyy-MM-dd HH:mm:ss Z"), dateFormatterOutputString: "yyyy-MM-dd")
                
                let servTime = getDateInCustmizeFormat("yyyy-MM-dd HH:mm:ss Z", dateString: getStringFromDate(filteredTime.first!.date, inputFormat: "yyyy-MM-dd HH:mm:ss Z"), dateFormatterOutputString: "hh:mm a")
                
                finalOrder.serviceDate = servDate
                finalOrder.serviceTime = servTime
                finalOrder.finalPrice = finalPrice
                finalOrder.addressId = deliveryAddress!.addressId
                finalOrder.deliveryAddress = self.deliveryAddress
                print("finalorderArray = \(placeOrderItemsArray), serviceDate = \(servDate), servicetime = \(servTime) finalprice= \(finalPrice) addressId = \(deliveryAddress?.addressId)")
                
                self.navigationController?.pushViewController(finalOrder, animated: true)
                
                
                
                
                
            }else{
                
                self.showAlertView(title: "Alert", message: "Please select the time for delivery")
                
            }
            
            
        }else{
            
            self.showAlertView(title: "Alert", message: "Please select the date for delivery")
        }
        
    
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        
        if collectionView.tag == DATETAG{
            
            count = dateArray.count
            
        }else{
            
            count = timeArray.count
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView.tag == DATETAG{
            
            let dateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as! DateCollectionViewCell
            let dateInArray = self.dateArray[indexPath.row]
            //daylbl
            dateCell.dayLabel.text = getDateInCustmizeFormat("yyyy-MM-dd HH:mm:ss Z", dateString: getStringFromDate(dateInArray, inputFormat: "yyyy-MM-dd HH:mm:ss Z"), dateFormatterOutputString: "E")
            
            //datelbl
            dateCell.dateLabel.text = getDateInCustmizeFormat("yyyy-MM-dd HH:mm:ss Z", dateString: getStringFromDate(dateInArray, inputFormat: "yyyy-MM-dd HH:mm:ss Z"), dateFormatterOutputString: "dd")
            
            let dateView = self.dateArrayN[indexPath.row]
            
            if dateView.selectedValue == 1{
                
                dateCell.coverContainerView.backgroundColor = .green
                
            }else{
                
                dateCell.coverContainerView.backgroundColor = .white
                
            }
            
            return dateCell
            
        }else{
            
            let timeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
            let dateInArray = self.timeArray[indexPath.row]
            
            timeCell.timeLabel.text = getDateInCustmizeFormat("yyyy-MM-dd HH:mm:ss Z", dateString: getStringFromDate(dateInArray, inputFormat: "yyyy-MM-dd HH:mm:ss Z"), dateFormatterOutputString: "hh:mm a")
            
            let timeView = self.timeArrayN[indexPath.row]
            
            if timeView.selectedValue == 1{
                
                timeCell.containerView.backgroundColor = .green
                
            }else{
                
                timeCell.containerView.backgroundColor = .white
                
            }
            
            
            
            return timeCell
            
        }
        
        //return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == DATETAG{
            
            return CGSize(width: 70, height: 55)
            
        }else{
            
            return CGSize(width: 159, height: 51)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView.tag == DATETAG{
            
            let selDate = dateArrayN[indexPath.row]
            
            //set all selected values to 0
            var count = 0
            for i in self.dateArrayN{
                
                var j = i
                j.selectedValue = 0
                
                self.dateArrayN[count] = j
                
                count = count + 1
                
                
            }
            
            if let row = self.dateArrayN.firstIndex(where: {$0.date == selDate.date
            }){
                var selItem = dateArrayN[row]
                selItem.selectedValue = 1
                
                dateArrayN[row] = selItem
                
            }
            
            self.selectDateCollectionView.reloadData()
            
        }else{
            
            
            let selDate = timeArrayN[indexPath.row]
            
            var count = 0
            for i in self.timeArrayN{
                
                var j = i
                j.selectedValue = 0
                
                self.timeArrayN[count] = j
                
                count = count + 1
                
                
            }
            
            if let row = self.timeArrayN.firstIndex(where: {$0.date == selDate.date
            }){
                var selItem = timeArrayN[row]
                selItem.selectedValue = 1
                
                timeArrayN[row] = selItem
                
            }
            
            self.selectTimeCollectionView.reloadData()
            
        }
        

        
        
    }
    
}
