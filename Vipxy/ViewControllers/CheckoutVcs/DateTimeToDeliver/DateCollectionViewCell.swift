//
//  DateCollectionViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 05/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var coverContainerView: UIView!
    
    override func awakeFromNib() {
    
        super.awakeFromNib()
        let yourColor : UIColor = UIColor(red:0.43, green:0.43, blue:0.43, alpha:1.0)
        self.coverContainerView.layer.borderWidth = 1
        self.coverContainerView.layer.borderColor = yourColor.cgColor
        self.coverContainerView.layer.cornerRadius = 5.0
        
        
    }
    
    
}
