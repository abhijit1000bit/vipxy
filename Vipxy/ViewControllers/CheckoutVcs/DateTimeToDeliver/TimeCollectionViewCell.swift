//
//  TimeCollectionViewCell.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 07/12/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class TimeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let yourColor : UIColor = UIColor(red:0.43, green:0.43, blue:0.43, alpha:1.0)
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.borderColor = yourColor.cgColor
        self.containerView.layer.cornerRadius = 5.0
        
    }
}
