//
//  BaseViewController.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 23/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class BaseViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func getDateInCustmizeFormat(_ dateFormatterInputString: String, dateString: String, dateFormatterOutputString:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatterInputString//"yyyy-MM-dd HH:mm:ss" //Your date format
        let date = dateFormatter.date(from: dateString) //according to date format your date string
        
        dateFormatter.dateFormat = dateFormatterOutputString //Your New Date format as per requirement change it own
        let newDate = dateFormatter.string(from: date!) //pass Date here
        //        print(newDate) //New formatted Date string
        
        return newDate
    }
    
    func getDateFromString(_ dateFormatterInputString: String, dateString: String) -> Date{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatterInputString//"yyyy-MM-dd HH:mm:ss" //Your date format
        let date = dateFormatter.date(from: dateString)
        
        return date!
        
        
    }
    
    func getStringFromDate(_ dateInput: Date, inputFormat:String)->String{
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = inputFormat
        
        let myString = formatter.string(from: dateInput)
        
        return myString
    }
    
    
    @objc func goBack()
    {
        if let navController = self.navigationController {
            
            navController.popViewController(animated: true)
            
        }
    }
    
    func showAlertView(title : String, message : String){
        let alertController = UIAlertController( title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        //you can add custom actions as well
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showCustomisedAlert(title: String, message: String){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.mainMessage = title
        myAlert.subMessage = message
        
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showOtpAlert(title:String){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "OtpAlertVc") as! OtpAlertVc
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //myAlert.mainMessage = title
        //myAlert.subMessage = message
        myAlert.alertTitle = title
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    func showMultiplePacksAlert(packArray:[Pack], itemId:String){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertForMultiplePacksVc") as! AlertForMultiplePacksVc
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //myAlert.mainMessage = title
        //myAlert.subMessage = message
        myAlert.packsArrayReceived = packArray
        myAlert.itemId = itemId
        self.present(myAlert, animated: true, completion: nil)
    }
    @objc func SideMenuAction()
    {
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }
    
    func showSideMenu(view: UIView) {
        // Define the menus
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let sideMenuObj : SideMenuViewController! = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        let menuLeftNavigationController = SideMenuNavigationController(rootViewController: sideMenuObj)
        menuLeftNavigationController.leftSide = true
        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        // let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController = menuLeftNavigationController
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        
        //        SideMenuManager.menuAddPanGestureToPresent(toView: view) //self.navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: SideMenuManager.PresentDirection(rawValue: 1)!)
        
        
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - (UIScreen.main.bounds.width/5)
        SideMenuManager.default.menuDismissOnPush = true
        //        SideMenuManager.menuEnableSwipeGestures = false
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        //SideMenuManager.default.menuPushStyle = .subMenu
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
    }
    
    
    func closeMenu()
    {
        dismiss(animated: true, completion: nil)
    }
    

    
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startLoading()
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        spinnerActivity?.labelText = "Loading";
        spinnerActivity?.detailsLabelText = "Please Wait!";
        spinnerActivity?.isUserInteractionEnabled = true;
    }
    
    func stopLoading()
    {
        MBProgressHUD.hide(for: self.view, animated: true);
    }
    
//    func showProgress() {
//        SVProgressHUD.show(withStatus: "Loading...")
//    }
//    
//    func dismissProgress() {
//        SVProgressHUD.dismiss()
//    }
    
}
