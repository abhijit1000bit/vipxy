//
//  SummaryVc.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 30/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation
import UIKit

class SummaryVc: BaseViewController, UITableViewDelegate, UITableViewDataSource, changeThePurchaseCountDelegate, oneAmongMultiplePackSelectedDelegate {
    
    
    func returnBackSelectedPack(itemId: String, packId: String, genderSelected: String, countReceived: Int) {
        
        if let row = self.chosenItemArray.firstIndex(where: {$0.itemId == itemId}) {
            var particularSelectedItem = chosenItemArray[row]
            particularSelectedItem.count = countReceived
            chosenItemArray[row] = particularSelectedItem
            
            if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
            }){
                var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                
                //if particularSelectedItem.count < countReceived{
                //plus clicked
                //print("original string = \(particularSelectedItem.multiplePackEncoding)")
                //particularSelectedItem.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                //particularSelectedItem.count = countReceived
                //itemList[row] = particularSelectedItem
                print("new string = \(particularSelectedItem.multiplePackEncoding)")
                
                particularItemInChosenArray.multiplePackEncoding.append("\(packId),\(genderSelected)X")
                particularItemInChosenArray.count = countReceived
                self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                //}
                self.updateSummaryView()
                self.itemsTableView.reloadData()
                
                
            }else{
                
                
                //particularSelectedItem.multiplePackEncoding = "\(packId),\(genderSelected)X"
                //particularSelectedItem.count = countReceived
                //itemList[row] = particularSelectedItem
                self.chosenItemArray.append(particularSelectedItem)
                print("multiple pack iutem added = \(particularSelectedItem)")
                self.updateSummaryView()
                self.itemsTableView.reloadData()
            }
            
            //            if particularSelectedItem.count > countReceived{
            //                //minus clicked
            //
            //
            //
            //            }
            //
            //            if particularSelectedItem.count < countReceived{
            //                //plus ckicked
            //
            //                //new entry
            //                if particularSelectedItem.count == 0{
            //                    //insert new item
            //                    //insert selected string
            //                    self.chosenItemArray.append(particularSelectedItem)
            //                    particularSelectedItem.multiplePackEncoding = "\(packId)\(genderSelected)X"
            //                    itemList[row] = particularSelectedItem
            //
            //                }else{
            //                    //modify the count
            //                    //modify the selected string array
            //
            //                }
            //
            //            }
            
            if chosenItemArray.count != 0{
                
                itemsTableView.isHidden = false
                
            }else{
                
                itemsTableView.isHidden = true
                updateSummaryView()
                self.itemsTableView.reloadData()
            }
            
            DispatchQueue.main.async {
                
                self.itemsTableView.reloadData()
            }
        }
        
        
        
    }
    
    
    
    func changePurchaseCount(itemId: String, itemCount: Int) {
        
        if let row = self.chosenItemArray.firstIndex(where: {$0.itemId == itemId}) {
            var particularSelectedItem = chosenItemArray[row]
            
            
            //If has multiple packs then do this as given below else follow what is normal i.e change the count and calculate the amount
            //MULTIPLE PACKS IN alertview display all the available packs , on chosing a pack append \(packid),\(M)X to
            //multiplePackSelectedString i.e packid and the male, female or either
            //\(packid),\(M)X\(packid),\(F)X\(packid),\(M)X\(packid),\(E)X == 23FX,24EX,24MX,23MX
            
            //Check for a minnus click, compare new count with the original if new is less than original then, remove the last
            //appended string from the components separatedby X array
            //Then crate a new string from components joined by the array
            //Then update the multiple pack encoding string
            
            if particularSelectedItem.hasDiffPack == "Yes"{
                
                //minus clicked
                if particularSelectedItem.count > itemCount{
                    
                    //check if itemCount  == 0
                    //if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                    //}){
                        //var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                        if itemCount == 0 {
                            
                            //particularSelectedItem.multiplePackEncoding = ""
                            //particularSelectedItem.count = itemCount
                            //itemList[row] = particularSelectedItem
                            self.chosenItemArray.remove(at: row)
                            self.updateSummaryView()
                            self.itemsTableView.reloadData()
                        }else{
                            
                            print("original string = \(particularSelectedItem.multiplePackEncoding)")
                            
                            var compAray = particularSelectedItem.multiplePackEncoding.components(separatedBy: "X")
                            
                            print("comp array original = \(compAray)")
                            
                            var last = compAray.removeLast()
                            
                            print("new comp array = \(compAray)")
                            
                            var secodLast = compAray.removeLast()
                            print("new comp array finally = \(compAray)")
                            var strinRep = compAray.joined(separator: "X")
                            strinRep.append("X")
                            print("new string = \(strinRep)")
                            particularSelectedItem.multiplePackEncoding = strinRep
                            
                            particularSelectedItem.count = itemCount
                            //itemList[row] = particularSelectedItem
                            //particularItemInChosenArray.multiplePackEncoding = strinRep
                            
                            //particularItemInChosenArray.count = itemCount
                            
                            self.chosenItemArray[row] = particularSelectedItem
                            self.updateSummaryView()
                            self.itemsTableView.reloadData()
                        }
                        
                    //}
                    
                    
                }else{
                    
                    //self.showMultiplePacksAlert(packArray: particularSelectedItem.packArray, itemId:itemId)
                    //Show an alert view controller and then sheow three radio buttons....
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let myAlert = storyboard.instantiateViewController(withIdentifier: "AlertForMultiplePacksVc") as! AlertForMultiplePacksVc
                    myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    //myAlert.mainMessage = title
                    //myAlert.subMessage = message
                    myAlert.packsArrayReceived = particularSelectedItem.packArray
                    myAlert.itemId = itemId
                    myAlert.purchaseDelegate = self
                    myAlert.countReceived = itemCount
                    self.present(myAlert, animated: true, completion: nil)
                    
                    let originalCount = particularSelectedItem.count
                    if originalCount == 0{
                        particularSelectedItem.count = 0
                        chosenItemArray[row] = particularSelectedItem
                        DispatchQueue.main.async {
                            
                            self.itemsTableView.reloadData()
                        }
                        
                    }
                    
                }
                
                
                
                
            }else{
                
                particularSelectedItem.count = itemCount
                chosenItemArray[row] = particularSelectedItem
                
                if let rowInChosenItemArray = self.chosenItemArray.firstIndex(where: { $0.itemId == itemId
                }){
                    var particularItemInChosenArray = self.chosenItemArray[rowInChosenItemArray]
                    if itemCount == 0{
                        
                        self.chosenItemArray.remove(at: rowInChosenItemArray)
                        self.updateSummaryView()
                        self.itemsTableView.reloadData()
                        
                    }else{
                        
                        particularItemInChosenArray.count = itemCount
                        self.chosenItemArray[rowInChosenItemArray] = particularItemInChosenArray
                        self.updateSummaryView()
                        self.itemsTableView.reloadData()
                    }
                    
                }else{
                    
                    self.chosenItemArray.append(particularSelectedItem)
                    self.updateSummaryView()
                    self.itemsTableView.reloadData()
                    
                }
                
            }
            
            
            
            //show hide of the summary view:
            //If the item count is not zero show the summary. view as soon as the item count is zero then hide the summary view after the item is removed from the array
            if chosenItemArray.count != 0{
                
                itemsTableView.isHidden = false
                
            }else{
                
                itemsTableView.isHidden = true
                updateSummaryView()
                self.itemsTableView.reloadData()
            }
            
            
        }

        
        
        
    }
    

    
    @IBOutlet weak var itemsTableView: UITableView!
    
    
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var discountLabel: UILabel!
 
    
    @IBOutlet weak var subTotalLbl: UILabel!
    
    var chosenItemArray: [Item] = [Item]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        itemsTableView.register(UINib.init(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        itemsTableView.rowHeight = UITableView.automaticDimension
        itemsTableView.estimatedRowHeight = 300
        updateSummaryView()
    }
    
    
    @IBAction func checkoutAndPaymentClicked(_ sender: Any) {
        
        //let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //let HomeViewController = storyBoard.instantiateViewController(withIdentifier: "LoginRegisterVc") as! LoginRegisterVc
        //self.navigationController!.pushViewController(HomeViewController, animated: false)
        
        
        if chosenItemArray.count != 0 {
            
            if Utility.getLoginStatus() {
                
                ///redirect to add new address vc
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let loginVc = storyBoard.instantiateViewController(withIdentifier: "ChooseServiceAddressVc") as! ChooseServiceAddressVc
                loginVc.chosenItemArray = self.chosenItemArray
                self.navigationController?.pushViewController(loginVc, animated: true)
                
            }else{
                
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let loginVc = storyBoard.instantiateViewController(withIdentifier: "LoginRegisterVc") as! LoginRegisterVc
                loginVc.chosenItemArray = self.chosenItemArray
                self.navigationController?.pushViewController(loginVc, animated: true)
                
            }
            
        }else{
            
            self.showAlertView(title: "", message: "Please select at least one item to order")
            
            
        }
        

        

        
    }
    
    func updateSummaryView(){
        
        //mainPurchaseCountLbl.text = "\(self.chosenItemArray.count)"
        
        if chosenItemArray.count != 0 {
            
            var total = 0
            for itm in self.chosenItemArray{
                
                if itm.hasDiffPack == "Yes"{
                    
                    var compAray = itm.multiplePackEncoding.components(separatedBy: "X")
                    
                    
                    var newCompArray = compAray.removeLast()
                    print("array of inserted string = \(compAray)")
                    
                    for selPack in compAray{
                        
                        var separatePackIdAndPref = selPack.components(separatedBy: ",")
                        print("array of inserted string = \(separatePackIdAndPref)")
                        let packIdS = separatePackIdAndPref[0]
                        let pref = separatePackIdAndPref[1]
                        if let rowInChosenPackArray = itm.packArray.firstIndex(where: { $0.packId == packIdS
                        }){
                            
                            let chosenPack = itm.packArray[rowInChosenPackArray]
                            total = total + (chosenPack.rate as! NSString).integerValue
                            
                        }
                        
                    }
                    
                }else{
                    
                    total = total + (itm.packArray.first?.rate as! NSString).integerValue*itm.count
                    
                }
                
                
                
                
            }
            totalLabel.text = "$\(total)"
            subTotalLbl.text = "$\(total)"
            discountLabel.text = "0.0"
            
        }else{
            totalLabel.text = "$ 0.0"
            subTotalLbl.text = "$ 0.0"
            discountLabel.text = "0.0"
            
        }
        
        
    }
    
    //MARK: UITABLEVIEW DELEGATE AND DATASOURCE METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chosenItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
        
        let item = chosenItemArray[indexPath.row]
        itemCell.model = ItemCellModel(item)
        itemCell.purchaseDelegate = self

        itemCell.selectionStyle = .none
        
        if item.hasDiffPack == "Yes"{
            var total = 0
            var compAray = item.multiplePackEncoding.components(separatedBy: "X")
            
            
            var newCompArray = compAray.removeLast()
            print("array of inserted string = \(compAray)")
            
            for selPack in compAray{
                
                var separatePackIdAndPref = selPack.components(separatedBy: ",")
                print("array of inserted string = \(separatePackIdAndPref)")
                let packIdS = separatePackIdAndPref[0]
                let pref = separatePackIdAndPref[1]
                if let rowInChosenPackArray = item.packArray.firstIndex(where: { $0.packId == packIdS
                }){
                    
                    let chosenPack = item.packArray[rowInChosenPackArray]
                    total = total + (chosenPack.rate as! NSString).integerValue
                    
                }
                
            }
            itemCell.cellTotalAmountLbl.text = "$ \(total)"
            
            
        }else{
            
            let total = (item.packArray.first?.rate as! NSString).integerValue*item.count
            itemCell.cellTotalAmountLbl.text = "$ \(total)"
            
        }
        
        
        return itemCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
