//
//  Utility.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 27/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation


class Utility: NSObject{
    
    //MARK: USER_ID
    class func setUserId(_ userId: String){
        
        UserDefaults.standard.set(userId, forKey: "userId")
        UserDefaults.standard.synchronize()
        
    }
    
    class func getuserId()->String{
        
        return UserDefaults.standard.string(forKey: "userId") ?? ""
    }
    
    
    //MARK: USERNAME
    class func setUserName(_ userName: String){
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.synchronize()
    }
    
    class func getUserName()->String{
        return UserDefaults.standard.string(forKey: "userName") ?? ""
    }
    
    //MARK: MOBILE
    class func setMobile(_ mobile: String){
        UserDefaults.standard.set(mobile, forKey: "mobile")
        UserDefaults.standard.synchronize()
    }
    
    class func getMobile()->String{
        return UserDefaults.standard.string(forKey: "mobile") ?? ""
    }
    
    //MARK: EMAIL
    class func setEmail(_ email: String){
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.synchronize()
        
    }
    
    class func getEmail()->String{
        return UserDefaults.standard.string(forKey: "email") ?? ""
        
        
    }
    
    //MARK: GET STATUS
    class func setStatus(_ status:String){
        UserDefaults.standard.set(status, forKey: "status")
        UserDefaults.standard.synchronize()
        
    }
    
    class func getStatus()->String{
        return UserDefaults.standard.string(forKey: "status") ?? ""
    }
    
    class func getLoginStatus() -> Bool {
        return (UserDefaults.standard.string(forKey: "loginStatus") == nil ? false : true)
    }
    
    class func setLoginStatus(){
        UserDefaults.standard.set(true, forKey: "loginStatus")
        UserDefaults.standard.synchronize()
    }
    
    
    //MARK: SAVE MSID
    class func setMainServiceId(_ msid: String){
        
        UserDefaults.standard.set(msid, forKey: "msId")
        UserDefaults.standard.synchronize()
    }
    
    class func getMainServicesId()->String{
        
        return UserDefaults.standard.string(forKey: "msId") ?? ""
        
    }
    
    
}
