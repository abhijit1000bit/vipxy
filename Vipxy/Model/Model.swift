//
//  Model.swift
//  Vipxy
//
//  Created by Harshal Bajaj on 23/11/19.
//  Copyright © 2019 Sagar. All rights reserved.
//

import Foundation

struct MainService {
    
    var msId : String
    var msName : String
    var msDescription : String
    var msImage : String
    
    init(json: [String:Any]) {
        
        msId = json["msid"] as? String ?? ""
        msName = json["ms_name"] as? String ?? ""
        msDescription = json["ms_descp"] as? String ?? ""
        msImage = json["ms_img"] as? String ?? ""
    }
    
}

struct Blog {

    var blogTitle : String
    var blogImage : String
    var blogId : String
    
    init(json: [String:Any]) {

        blogTitle = json["blog_title"] as? String ?? ""
        blogImage = json["blog_img"] as? String ?? ""
        blogId = json["bid"] as? String ?? ""
        
    }
    
}

struct Service {
    
    
    var sid:String
    var sName:String
    var sImage:String
    var hasSubServicesThenitem: String
    
    init(json: [String:Any]) {
        
        sid = json["sid"] as? String ?? ""
        sName = json["s_name"] as? String ?? ""
        sImage = json["s_img"] as? String ?? ""
        hasSubServicesThenitem = json["has_subservices_then_item"] as? String ?? ""
        
 
    }
    
    
}
struct OuterJsonSearchItems: Codable {
    var messageCode: Int
    var message: String
    //var servicDetails:serviceDetails
    var list:[Item]
    
    enum CodingKeys: String, CodingKey {
        case messageCode = "message_code"
        case message = "message"
        //case servicDetails = "s_details"
        case list = "list"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        messageCode = try container.decode(Int.self, forKey: .messageCode)
        list = try container.decode([Item].self, forKey: .list)
        message = try container.decode(String.self, forKey: .message)
        //servicDetails = try container.decode(serviceDetails.self, forKey: .servicDetails)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //try container.encode(self.servicDetails, forKey: .servicDetails)
        try container.encode(self.list, forKey: .list)
        try container.encode(self.message, forKey: .message)
        try container.encode(self.messageCode, forKey: .messageCode)
    }
    
    
}

struct OuterJsonForAddressList:Codable{
    
    var messageCode: Int
    var message: String
    //var servicDetails:serviceDetails
    var list:[Address]
    enum CodingKeys: String, CodingKey {
        case messageCode = "message_code"
        case message = "message"
        //case servicDetails = "s_details"
        case list = "list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        messageCode = try container.decode(Int.self, forKey: .messageCode)
        list = try container.decode([Address].self, forKey: .list)
        message = try container.decode(String.self, forKey: .message)
        //servicDetails = try container.decode(serviceDetails.self, forKey: .servicDetails)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //try container.encode(self.servicDetails, forKey: .servicDetails)
        try container.encode(self.list, forKey: .list)
        try container.encode(self.message, forKey: .message)
        try container.encode(self.messageCode, forKey: .messageCode)
    }
}



struct Address:Codable {
    var addressId:String
    var tag:String
    var location:String
    var houseNumber:String
    var landMark:String
    
    
    enum CodingKeys: String, CodingKey {
        case addressId = "add_id"
        case tag = "tag"
        case location = "location"
        case houseNumber = "house_no"
        case landMark = "landmark"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        addressId = try container.decode(String.self, forKey: .addressId)
        tag = try container.decode(String.self, forKey: .tag)
        location = try container.decode(String.self, forKey: .location)
        houseNumber = try container.decode(String.self, forKey: .houseNumber)
        landMark = try container.decode(String.self, forKey: .landMark)
        
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.addressId, forKey: .addressId)
        try container.encode(self.tag, forKey: .tag)
        try container.encode(self.location, forKey: .location)
        try container.encode(self.houseNumber, forKey: .houseNumber)
        try container.encode(self.landMark, forKey: .landMark)
    }
}





struct OuterJsonItems: Codable {
    var messageCode: Int
    var message: String
    var servicDetails:serviceDetails
    var list:[Item]
    
    enum CodingKeys: String, CodingKey {
        case messageCode = "message_code"
        case message = "message"
        case servicDetails = "s_details"
        case list = "list"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        messageCode = try container.decode(Int.self, forKey: .messageCode)
        list = try container.decode([Item].self, forKey: .list)
        message = try container.decode(String.self, forKey: .message)
        servicDetails = try container.decode(serviceDetails.self, forKey: .servicDetails)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.servicDetails, forKey: .servicDetails)
        try container.encode(self.list, forKey: .list)
        try container.encode(self.message, forKey: .message)
        try container.encode(self.messageCode, forKey: .messageCode)
    }
    
    
}


struct OuterJsonSubserviceThenItems: Codable{
    var messageCode: Int
    var message: String
    var servicDetails:serviceDetails
    var list:[subServicesThenItem]
    
    enum CodingKeys: String, CodingKey {
        case messageCode = "message_code"
        case message = "message"
        case servicDetails = "s_details"
        case list = "list"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        messageCode = try container.decode(Int.self, forKey: .messageCode)
        list = try container.decode([subServicesThenItem].self, forKey: .list)
        message = try container.decode(String.self, forKey: .message)
        servicDetails = try container.decode(serviceDetails.self, forKey: .servicDetails)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.servicDetails, forKey: .servicDetails)
        try container.encode(self.list, forKey: .list)
        try container.encode(self.message, forKey: .message)
        try container.encode(self.messageCode, forKey: .messageCode)
    }
    
}


struct OuterJsonSubserviceThenItemsWithAddOn: Codable{
    var messageCode: Int
    var message: String
    var servicDetails:serviceDetails
    var list:[subServicesThenItem]
    var addOnList:[Item]
    
    enum CodingKeys: String, CodingKey {
        case messageCode = "message_code"
        case message = "message"
        case servicDetails = "s_details"
        case list = "list"
        case addOnList = "addon"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        messageCode = try container.decode(Int.self, forKey: .messageCode)
        list = try container.decode([subServicesThenItem].self, forKey: .list)
        message = try container.decode(String.self, forKey: .message)
        servicDetails = try container.decode(serviceDetails.self, forKey: .servicDetails)
        addOnList = try container.decode([Item].self, forKey: .addOnList)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.servicDetails, forKey: .servicDetails)
        try container.encode(self.list, forKey: .list)
        try container.encode(self.message, forKey: .message)
        try container.encode(self.messageCode, forKey: .messageCode)
        try container.encode(self.addOnList, forKey: .addOnList)
        
    }
    
}





struct serviceDetails: Codable{
    var serviceName: String
    var hasSubServicesThenItem: String
    var hasAddOn:String
    
    enum CodingKeys: String, CodingKey{
        case serviceName = "s_name"
        case hasSubServicesThenItem = "has_subservices_then_item"
        case hasAddOn = "has_addon"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        serviceName = try container.decode(String.self, forKey: .serviceName)
        hasSubServicesThenItem = try container.decode(String.self, forKey: .hasSubServicesThenItem)
        hasAddOn = try container.decode(String.self, forKey: .hasAddOn)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.serviceName, forKey: .serviceName)
        try container.encode(self.hasSubServicesThenItem, forKey: .hasSubServicesThenItem)
        try container.encode(self.hasAddOn, forKey: .hasAddOn)
       
    }
}

struct subServicesThenItem: Codable {
    
    var subId:String
    var subName:String
    var itemArray:[Item]
    enum CodingKeys: String, CodingKey{
        case subId = "subid"
        case subName = "sub_name"
        case itemArray = "items"
        
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        subId = try container.decode(String.self, forKey: .subId)
        itemArray = try container.decode([Item].self, forKey: .itemArray)
        subName = try container.decode(String.self, forKey: .subName)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.subId, forKey: .subId)
        try container.encode(self.itemArray, forKey: .itemArray)
        try container.encode(self.subName, forKey: .subName)
    }
    
//    init(json: [String:Any]) {
//
//        subId = json["subid"] as? String ?? ""
//        subName = json["sub_name"] as? String ?? ""
//        itemArray = json["items"] as? [Item] ?? []
//    }
    
    
}


struct Item: Codable{
    
    var itemId: String
    var itemName: String
    var itemDescription: String
    var hasDiffPack: String
    var itemImage:String?
    var packArray:[Pack]
    var type:String
    var count:Int
    var multiplePackEncoding:String
    
    enum CodingKeys: String, CodingKey{
        case itemId = "itemid"
        case itemName = "i_name"
        case itemDescription = "i_descp"
        case hasDiffPack = "has_diff_pack"
        case itemImage = "i_img"
        case packArray = "pack"
        case type = "type"
        case count = "count"
        case multiplePackEncoding = "multiplePackEncoding"
    }
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        itemId = try container.decode(String.self, forKey: .itemId)
        itemName = try container.decode(String.self, forKey: .itemName)
        itemDescription = try container.decode(String.self, forKey: .itemDescription)
        hasDiffPack = try container.decode(String.self, forKey: .hasDiffPack)
        itemImage = try? container.decode(String.self, forKey: .itemImage)
        packArray = try container.decode([Pack].self, forKey: .packArray)
        type = try container.decode(String.self, forKey: .type)
        count = 0
        multiplePackEncoding = ""
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.itemId, forKey: .itemId)
        try container.encode(self.itemName, forKey: .itemName)
        try container.encode(self.itemDescription, forKey: .itemDescription)
        
        try container.encode(self.hasDiffPack, forKey: .hasDiffPack)
        try container.encode(self.itemImage, forKey: .itemImage)
        try container.encode(self.packArray, forKey: .packArray)
        try container.encode(self.type, forKey: .type)
        try container.encode(self.count, forKey: .count)
        try container.encode(self.multiplePackEncoding, forKey: .multiplePackEncoding)
        
    }
//    init(json: [String:Any]) {
//
//        itemId = json["itemid"] as? String ?? ""
//        itemName = json["i_name"] as? String ?? ""
//        itemDescription = json["i_descp"] as? String ?? ""
//        hasDiffPack = json["has_diff_pack"] as? String ?? ""
//        itemImage = json["i_img"] as? String ?? ""
//        packArray = json["pack"] as? [Pack] ?? []
//    }
    
}

struct Pack : Codable{
    
    var packId: String
    var rate:String
    var min:String
    var selected:Int
    
    enum CodingKeys: String, CodingKey{
        case packId = "pckid"
        case rate = "rate"
        case min = "min"
        case selected = "selected"
    }

    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        packId = try container.decode(String.self, forKey: .packId)
        rate = try container.decode(String.self, forKey: .rate)
        min = try container.decode(String.self, forKey: .min)
        selected = 0
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.packId, forKey: .packId)
        try container.encode(self.rate, forKey: .rate)
        try container.encode(self.min, forKey: .min)
        try container.encode(self.selected, forKey: .selected)
        
    }
}

struct DateTimeSelected{
    
    var date:Date
    var selectedValue:Int
    
    init(dateS:Date, selValue:Int) {
        
        self.date = dateS
        self.selectedValue = selValue
    }
}

struct placeOrderItem{
    
    var itemId:String
    var packId:String
    var count:String
    var multiplePackEncoding:String
    
    init(itemID:String, packID:String, count:String, multiplePackEncoding:String) {
        
        self.itemId = itemID
        self.packId = packID
        self.count = count
        self.multiplePackEncoding = multiplePackEncoding
    }
}









